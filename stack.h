/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   stack.h
 * Author: temporal
 *
 * Created on 5 de octubre de 2017, 17:08
 */

#ifndef STACK_H
#define STACK_H
#include <stdio.h>
#include "generic_collections_types.h"
#include "basic_types.h"
#include "generic_string.h"

typedef struct _Stack Stack;
/**
 * @brief Inicializa​la pila reservando​memoria y almacenando lostres​ ​ punteros​ ​ afunción pasados como​
 * parámetro (el primero para destruir​elementos, el segundo para copiar elementos​y el tercero​para
 * imprimir​elementos).
 * @param 
 */
Stack* stack_ini(destroy_element_function_type, copy_element_function_type, 
        print_element_function_type, cmp_element_function_type);
/**
 * @brief Eliminar la pila
 * @param la pila para eliminar
 */
void stack_destroy(Stack *pila);
/**
 * @brief Comprueba si la pila es vacia
 * @param pila
 * @return 
 */
Bool stack_isEmpty(const Stack* pila);
/**
 * @brief Comprueba si la pila es llena
 * @param pila
 * @return 
 */
Bool stack_isFull(const Stack* pila);
/**
 * @brief Meter un elemento en la pila
 * @param pila
 * @param ele
 * @return 
 */
Stack* stack_push(Stack* pila , const void* ele);
/**
 * @brief Sacar un elemento de la pila
 * @param pila
 * @return 
 */
void* stack_pop(Stack* pila);
/**
 * @brief Devolver la copia del elemento en el tope de pila
 * @param pila
 * @return 
 */
void* stack_top(const Stack* pila);
/**
 * @brief muestra la pila
 * @param fd
 * @param pila
 * @return 
 */
int stack_print(FILE* fd, const Stack* pila);
/**
 * @brief devolver el tamano de la pila
 * @param pila
 * @return 
 */
int stack_size(const Stack* pila);
/**
 * @brief devolver la copia de la pila
 * @param pila
 * @return 
 */
Stack*  stack_copy(Stack* pila);
/**
 * @brief compara dos pila
 * @param s1
 * @param s2
 * @return 
 */
int stack_compare(Stack* s1, Stack* s2);

#ifdef __cplusplus
extern "C" {
#endif




#ifdef __cplusplus
}
#endif

#endif /* STACK_H */

