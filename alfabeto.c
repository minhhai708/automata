#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "alfabeto.h"

struct _Alfabeto {
    char * nombre;
    int tamano;
    char ** simbolos;
};

Alfabeto * alfabetoNuevo(char * nombre, int tamano) {
    Alfabeto* alfabeto;
    alfabeto = (Alfabeto*) malloc(sizeof (Alfabeto));

    alfabeto->nombre = (char *) malloc(sizeof (char)* (strlen(nombre) + 1));
    strcpy(alfabeto->nombre, nombre);
    alfabeto->tamano = tamano;

    alfabeto->simbolos = (char **) malloc(sizeof (char*) * tamano);
    for (int i = 0; i < alfabeto->tamano; ++i) {
        alfabeto->simbolos[i] = NULL;
    }
    return alfabeto;
}

void alfabetoElimina(Alfabeto * p_alfabeto) {
    if (!p_alfabeto) return;

    for (int i = 0; i < p_alfabeto->tamano; i++) {
        if (p_alfabeto->simbolos[i] != NULL)
            free(p_alfabeto->simbolos[i]);
    }

    free(p_alfabeto->simbolos);
    free(p_alfabeto->nombre);
    free(p_alfabeto);

    return;
}

Alfabeto * alfabetoInsertaSimbolo(Alfabeto * p_alfabeto, char * simbolo) {
    if (!p_alfabeto) return NULL;

    for (int i = 0; i < p_alfabeto->tamano; i++) {
        if (p_alfabeto->simbolos[i] == NULL) {
            p_alfabeto->simbolos[i] = (char*) malloc((strlen(simbolo) + 1) * sizeof (char));
            strcpy(p_alfabeto->simbolos[i], simbolo);
            return p_alfabeto;
        }
    }


    return p_alfabeto;
}

void alfabetoImprime(FILE * fd, Alfabeto * p_alf) {
    if (!p_alf) return;
    fprintf(stdout, "%s", p_alf->simbolos[0]);
    for (int i = 1; i < p_alf->tamano; ++i) {
        fprintf(stdout, ",%s", p_alf->simbolos[i]);
    }
    printf("\n");
}

char* alfabetoSimboloEn(Alfabeto * p_alf, int i) {
    if (!p_alf) return NULL;
    if (i < p_alf->tamano) return p_alf->simbolos[i];
    else return NULL;
}

int alfabetoIndiceDeSimbolo(Alfabeto * p_alf, char * simbolo) {
    if (!p_alf || !simbolo) return -1;
    for (int i = 0; i < p_alf->tamano; ++i) {
        if (p_alf->simbolos[i] != NULL) {
            if (strcmp(p_alf->simbolos[i], simbolo) == 0) return i;
        }
    }
    return -1;
}

int alfabetoTamano(Alfabeto * p_alf) {
    if (!p_alf) return -1;
    return p_alf->tamano;

}
/* PARA LA PRÁCTICA 3 */
Alfabeto * alfabetoUne(Alfabeto * p_a1, Alfabeto * p_a2);