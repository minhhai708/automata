
/* 
 * File:   configuracion_ap.h
 * Author: MINH HAI LE
 *
 * Created on 19 de octubre de 2017, 16:51
 */

#ifndef CONFIGURACION_AP_H
#define CONFIGURACION_AP_H

#ifdef __cplusplus
extern "C" {
#endif
#include "estado.h"
#include "palabra.h"
#include "stack.h"
    
    typedef struct _ConfiguracionAp ConfiguracionAp;
    /**
     * @brief Crear nueva configuracion de AP
     * @param estado 
     * @param pila
     * @param cadena
     * @return 
     */
    ConfiguracionAp* configuracionApNueva(Estado*estado, Stack*pila, Palabra* cadena);
    /**
     * @brief Devolver la palabra de la configuracion
     * @param ap
     * @return 
     */

    void configuracionApElimina(ConfiguracionAp * p_cap);
    /**
     * @brief Imprimir la configuracion en la salida indicada
     * @param fd la salida
     * @param p_cap la configuracion
     */
    void configuracionApImprime(FILE*fd, ConfiguracionAp*p_cap);
    /**
     * @brief Devolver una copia de la configuracion
     * @param p_cap
     * @return la copia
     */
    ConfiguracionAp* configuracionApCopia(ConfiguracionAp*p_cap);

    /**
     * @brief Comparar dos configuraciones
     * @param p_cap1
     * @param p_cap2
     * @return 
     */
    int configuracionCompara(ConfiguracionAp* p_cap1, ConfiguracionAp*p_cap2);
    
    /**
     * @brief Devolver la palabra de la configuracion
     * @param ap
     * @return 
     */

    Palabra* configuracionApGetPalabra(ConfiguracionAp* ap);

    /**
     * @brief Devolver la pila de la configuracion
     * @param ap
     * @return 
     */
    Stack* configuracionApGetPila(ConfiguracionAp* ap);

    /**
     * @brief Devolver el estado de la configuracion
     * @param ap
     * @return 
     */
    Estado* configuracionApGetEstado(ConfiguracionAp* ap);
#ifdef __cplusplus
}
#endif

#endif /* CONFIGURACION_AP_H */

