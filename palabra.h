/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   palabra.h
 * Author: MINH HAI LE
 *
 * Created on 19 de octubre de 2017, 16:51
 */

#ifndef PALABRA_H
#define PALABRA_H

#ifdef __cplusplus
extern "C" {
#endif
#include <stdio.h>
#include <stdlib.h>
#include "generic_collections_types.h"
#include "basic_types.h"
#include "generic_string.h"

    typedef struct _Palabra Palabra;
    /**
     * @brief Reserva memoria para palabra nueva
     * @return 
     */
    Palabra * palabraNueva();

    /**
     * @brief Libera la memobria asociada con palabra
     * @param pPalabra
     */
    void palabraElimina(Palabra* p_p);

    /**
     * @brief Muestra por el file la palabra
     * @param fd
     * @param pPalabra
     */
    void palabraImprime(FILE * fd, Palabra * p_p);


    /**
     * @brief Insertar letra a la palabra
     * @param pPalabra
     * @param letra
     * @return 
     */
    Palabra * palabraInsertaLetra(Palabra * p_p, char * letra);


    /**
     * @brief devolver tamano de la palabra
     * @param pPalabra
     * @return 
     */
    int palabraTamano(Palabra * p_p);


    /**
     * @brief Hacer en memoria nueva una copia de la palabra y devuelve
     * @param pPalabra
     * @return la copia de la plabra
     */
    Palabra * palabraCopia(Palabra * p_p);


    /**
     * @brief Compara dos palabras
     * @param pPalabra1
     * @param pPalabra2
     * @return 
     */
    int palabraCompara(Palabra * p_p1, Palabra * p_p2);


    /**
     * @brief Devlover la letra por una posicion dada
     * @param p
     * @param pos
     * @return 
     */
    char* palabraEnPosicion(Palabra* p, int pos);


    /**
     * @brief Comprueba si no hay letra en la palabra
     * @param p
     * @return 
     */
    Bool palabra_is_empty(Palabra *p);

#ifdef __cplusplus
}
#endif

#endif /* PALABRA_H */

