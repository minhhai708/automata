/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   generic_string..h
 * Author: temporal
 *
 * Created on 5 de octubre de 2017, 17:08
 */

#ifndef GENERIC_STRING__H
#define GENERIC_STRING__H
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#ifdef __cplusplus
extern "C" {
#endif
#define ELESIZE sizeof(char)
    int print_p_string(FILE*, char*);

    char* copy_p_string(char*);

    void destroy_p_string(char*);

    int cmp_p_string(char*, char*);

#ifdef __cplusplus
}
#endif

#endif /* GENERIC_STRING__H */

