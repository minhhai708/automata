/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   matrix.h
 * Author: hai
 *
 * Created on November 16, 2017, 3:07 PM
 */

#ifndef MATRIX_H
#define MATRIX_H

#ifdef __cplusplus
extern "C" {
#endif
#include <stdio.h>
#include <stdlib.h>
#include "basic_types.h"

    typedef struct _Matrix Matrix;
    /**
     * @brief reserva y Crear una matriz vacia con el valor de todas las posicion son ceros 
     * @param size
     * @return 
     */
    Matrix* create_Zeromatrix(int size);
    /**
     * @brief Muestra la matrix
     * @param fd
     * @param matrix
     */
    void print_matrix(FILE* fd, Matrix* matrix);
    /**
     * @brief Liberar memoria reservada para matrix
     * @param matrix
     */
    void destroy_matrix(Matrix* matrix);
    /**
     * @brief Set valor para una posicion de matrix
     * @param matrix
     * @param fila
     * @param col
     * @param value
     */
    void setValue_by_position_matrix(Matrix* matrix, int fila, int col, int value);
    /**
     * @brief Delvolver la copia de matriz
     * @param matrix
     * @return 
     */
    Matrix* copy_matrix(Matrix* matrix);
    /**
     * @brief Copia valores de dos matrix tiene mismo tamano
     * @param m1
     * @param m2
     */
    void copy_value_matrix(Matrix* m1, Matrix* m2);
    /**
     * @brief Compara valor de dos matrix
     * @param m1
     * @param m2
     * @return 
     */
    Bool is_same_value_matrix(Matrix* m1, Matrix* m2);
    /**
     * @brief return power of a matrix
     * @param m
     * @param p
     * @return 
     */
    Matrix* power_of_matrix(Matrix* m, int p);
    /**
     * @brief Devolver valor en una posicion de matrix
     * @param m
     * @param col
     * @param row
     * @return 
     */
    int getValueByPosition_matrix(Matrix* m, int col, int row);
    /**
     * @brief Return union of two matrix
     * @param m1
     * @param m2
     * @return 
     */
    Matrix* union_of_matrix(Matrix* m1, Matrix* m2);

#ifdef __cplusplus
}
#endif

#endif /* MATRIX_H */

