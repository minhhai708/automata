#ifndef AFND_H
#define AFND_H

#include <stdio.h>

#include "estado.h"
#include "palabra.h"
#include "stack.h"
#include "configuracion_apnd.h"
#include "configuracion_ap.h"
#include "transicion_ap.h"

#define SIMBOLO_INICIO_PILA "Z"
#define ENTRADA_LAMDA "LAMDA"

typedef int* VectorIndices;

void VectorIndicesImprime(FILE *fd, VectorIndices vi, int tamano);
int VectorIndicesVacio(VectorIndices vi, int tamano);

typedef struct _AP AP;
/**
 * CREAR NUEVA AUTOMATA PILA 
 * @param nombre 
 * @param num_estados
 * @param num_simbolos_entrada
 * @param num_simbolos_pila
 * @return 
 */
AP * APNuevo(char * nombre, int num_estados, int num_simbolos_entrada, int num_simbolos_pila);
/**
 * ELIMINAR UN AUTOMATA PILA
 * @param p_ap
 */
void APElimina(AP * p_ap);
/**
 * IMPRIMIR LOS COMPONENTES DE LA AUTOMATA
 * @param fd
 * @param p_ap
 */
void APImprime(FILE * fd, AP* p_ap);
/**
 * INSERTAR NUEVO SIMBOLO AL ALFABETO ENTRADA
 * @param p_ap
 * @param simbolo
 * @return 
 */
AP * APInsertaSimboloAlfabetoEntrada(AP * p_ap, char * simbolo);
/**
 * INSERTAR NUEVO SIMBOLO AL ALFABETO PILA
 * @param p_ap
 * @param simbolo
 * @return 
 */
AP * APInsertaSimboloAlfabetoPila(AP * p_ap, char * simbolo);
/**
 * INSERTAR UN ESTADO A LA AUTOMATA
 * @param p_ap
 * @param nombre
 * @param tipo
 * @return NULL si no tiene exito, puntero al AUTOMATA si tiene exito
 */
AP * APInsertaEstado(AP * p_ap, char * nombre, int tipo);
/**
 * dado un automata pila, devolver nombre de estado un una posicion
 * @param p_ap
 * @param pos
 * @return nombre de estado
 */
char * APNombreEstadoEn(AP * p_ap, int pos);
/**
 * dado un automata pila, devolver nombre simbolo de pila en una posicion
 * @param p_ap
 * @param pos
 * @return 
 */
char * APSimboloEntradaEn(AP * p_ap, int pos);
/** 
 * dado un automata pila, devolver nombre simbolo de pila en una posicion
 * @param p_ap
 * @param pos
 * @return 
 */
char * APSimboloPilaEn(AP * p_ap, int pos);
/**
 * 
 * @param p_ap
 * @param nombre
 * @return 
 */
int APIndiceDeSimboloEntrada(AP * p_ap, char * nombre);
/**
 * 
 * @param p_ap
 * @param nombre
 * @return 
 */
int APIndiceDeSimboloPila(AP * p_ap, char * nombre);
/**
 * 
 * @param p_ap
 * @param nombre_estado_i
 * @param nombre_estado_f
 * @return 
 */
int APIndiceDeEstado(AP * p_ap, char * nombre);
/**
 * 
 * @param p_ap
 * @param nombre_estado_i
 * @param nombre_estado_f
 * @return 
 */
AP * APInsertaLTransicion(AP * p_ap, char * nombre_estado_i, char * nombre_estado_f);
/**
 * 
 * @param p_ap
 * @param nombre_simbolo_pila
 * @param nombre_estado_i
 * @param nombre_estado_f
 * @param nombre_simbolo_entrada
 * @param accion
 * @return 
 */
AP * APInsertaTransicion(AP * p_ap, char * nombre_simbolo_pila, char * nombre_estado_i, char * nombre_estado_f, char* nombre_simbolo_entrada, Palabra * accion);
/**
 * 
 * @param fd
 * @param p_ap
 */
void APImprimeConfiguracionApndActual(FILE * fd, AP * p_ap);
/**
 * 
 * @param fd
 * @param p_ap
 */
void APImprimeCadenaActual(FILE *fd, AP * p_ap);
/**
 * 
 * @param p_ap
 * @return 
 */
AP * APCierraLTransicion(AP * p_ap);
/**
 * 
 * @param p_ap
 * @return 
 */
AP * APInicializaCadena(AP * p_ap);
/**
 * 
 * @param p_ap
 * @return 
 */
int APIndiceEstadoInicial(AP * p_ap);
/**
 * 
 * @param p_ap
 * @return 
 */
AP * APInicializaEstado(AP * p_ap);
/**
 * 
 * @param p_ap
 * @param pos
 * @return 
 */
Estado * APEstadoEn(AP * p_ap, int pos);
/**
 * 
 * @param p_ap
 * @param nombre
 * @return 
 */
Estado * APEstadoDeNombre(AP *p_ap, char * nombre);
/**
 * 
 * @param p_ap
 * @param i
 * @param j
 * @return 
 */
int APCierreLTransicionIJ(AP * p_ap, int i, int j);
/**
 * 
 * @param p_ap
 * @param letra
 * @return 
 */
AP * APInsertaLetra(AP * p_ap, char * letra);
/**
 * 
 * @param p_ap
 * @param conf
 * @param di
 * @return 
 */
ConfiguracionApnd * APInsertaCierreLambda(AP * p_ap, ConfiguracionApnd * conf, ConfiguracionAp * di);
/**
 * 
 * @param pila
 * @param accion
 * @return 
 */
Stack * APInsertaAccionStack(Stack * pila, Palabra * accion);
/**
 * 
 * @param p_ap
 * @return 
 */
int APTransita(AP * p_ap);
/**
 * 
 * @param fd
 * @param p_ap
 * @return 
 */
int APProcesaEntrada(FILE *fd, AP * p_ap);


#endif
