/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include "estado.h"

struct _Estado {
    char *nombre;
    int tipo;
};

/**
 * Crear un estado nuevo
 * @param nombre
 * @param tipo
 * @return 
 */
Estado* estadoNuevo(const char* nombre, int tipo) {
    Estado* nEstado;
    
    nEstado = (Estado *) malloc(sizeof (Estado));
    if (!nEstado) return NULL;
    
    nEstado->nombre = (char *) malloc(sizeof (char)*(strlen(nombre) + 1));
    memcpy(nEstado->nombre, nombre, strlen(nombre) + 1);
    nEstado->tipo = tipo;
    
    return nEstado;
}

/**
 * Eliminar un estado y liberar su memoria
 * @param p_s Estado
 */
void estadoElimina(Estado* p_s) {
    if (!p_s)
        return;
    free(p_s->nombre);
    free(p_s);
    return;
}

/**
 * Imprimir el estado 
 * @param fd
 * @param p_s
 */
void estadoImprime(FILE* fd, Estado* p_s) {
    if (!p_s) return;
   /* fprintf(fd, "ESTADO%s\tTIPO: %d\n", p_s->nombre, p_s->tipo);*/
    fprintf(fd, "%s ", p_s->nombre);
    return;
}

/**
 * Función de comparación de estados por nombre, devuelve 1 si el estado tiene el nombre proporcionado
 * @param p_s el estado
 * @param nombre el nombre
 * @return 
 */

int estadoEs(Estado * p_s, char* nombre) {
    if (!p_s || !nombre)
        return 0;
    if (strcmp(p_s->nombre, nombre) == 0) return 1;
    else return 0;
}

/**
 * Dado un estado y devolver su nombre
 * @param p_s el estado
 * @return nombre del estado
 */
char * estadoNombre(Estado* p_s) {
    if (!p_s) return NULL;
    else return p_s->nombre;
}

/**
 * Dado un estado y devolver su tipo
 * @param p_s
 * @return el tipo del estado
 */
int estadoTipo(Estado * p_s) {
    if (!p_s) return -1;
    else return p_s->tipo;
}

/**
 * Se usa para comparar estados por su nombre
 * @param p_s1
 * @param p_s2
 * @return el strcmp de sus nombres
 */
int estadoCompara(Estado * p_s1, Estado * p_s2) {
    if (!p_s1 && !p_s2) return 0;
    else if (!p_s1 && p_s2) return -1;
    else if (p_s1 && !p_s2) return 1;
    else {
        return strcmp(p_s1->nombre, p_s2->nombre);
    }

}

/**
 * Se crea una copia en memoria nueva del estado proporcionado como argumento
 * @param p_s
 * @return la copia del estado
 */
Estado * estado_copy(Estado * p_s) {
    Estado *copyEstado;
    if (!p_s) return NULL;
    copyEstado = estadoNuevo(p_s->nombre, p_s->tipo);
    return copyEstado;
}