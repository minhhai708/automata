/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "palabra.h"

struct _Palabra {
    char** cadena;
    int tamano;
};

/**
 * 
 * @return 
 */
Palabra * palabraNueva() {
    Palabra * newWord;
    newWord = (Palabra*) malloc(sizeof (Palabra));
    if (!newWord) return NULL;
    newWord->cadena = (char **) malloc(sizeof (char*));
    newWord->tamano = 0;
    return newWord;
}

/**
 * Libera la memobria asociada con palabra
 * @param pPalabra
 */
void palabraElimina(Palabra* pPalabra) {
    int i;
    if (!pPalabra) return;
    for (i = 0; i < pPalabra->tamano; i++) {
        free(pPalabra->cadena[i]);
    }
    free(pPalabra->cadena);
    free(pPalabra);
    return;
}

/**
 * @brief Muestra por el file la palabra
 * @param fd
 * @param pPalabra
 */
void palabraImprime(FILE * fd, Palabra * pPalabra) {
    int i;
    if (!fd || !pPalabra) return;
    fprintf(fd, "(%i)", pPalabra->tamano);
    for (i = 0; i < pPalabra->tamano; i++) {
        if (pPalabra->cadena[i])
            fprintf(fd, "%s ", pPalabra->cadena[i]);
    }
}

/**
 * Insertar letra a la palabra
 * @param pPalabra
 * @param letra
 * @return 
 */

Palabra* palabraInsertaLetra(Palabra* pPalabra, char* letra) {
    char** newItem;
    if (!pPalabra || !letra) return NULL;
    newItem = (char **) realloc(pPalabra->cadena, (pPalabra->tamano + 1) * sizeof (char*));
    if (newItem) {
        char* newEle;
        pPalabra->cadena = newItem;
        newEle = (char *) malloc((strlen(letra) + 1) * sizeof (char));
        if (newEle) {
            pPalabra->cadena[pPalabra->tamano] = newEle;
            memcpy(pPalabra->cadena[pPalabra->tamano], letra, strlen(letra) + 1);
            pPalabra->tamano++;
        }
    }
    return pPalabra;
}

/**
 * devolver tamano de la palabra
 * @param pPalabra
 * @return 
 */
int palabraTamano(Palabra * pPalabra) {
    if (!pPalabra) return ERROR;
    else return pPalabra->tamano;
}

/**
 * Hacer en memoria nueva una copia de la palabra y devuelve
 * @param pPalabra
 * @return la copia de la plabra
 */
Palabra * palabraCopia(Palabra * pPalabra) {
    Palabra* pNew = NULL;
    int i;
    if (!pPalabra) return NULL;
    pNew = palabraNueva();
    if (!pNew) return NULL;
    for (i = 0; i < pPalabra->tamano; i++) {
        pNew = palabraInsertaLetra(pNew, pPalabra->cadena[i]);
    }
    return pNew;
}

/**
 * Compara dos palabras
 * @param pPalabra1
 * @param pPalabra2
 * @return 
 */
int palabraCompara(Palabra * pPalabra1, Palabra * pPalabra2) {
    if (!pPalabra1 && !pPalabra2) return 0;
    else if (!pPalabra1 && pPalabra2) return -1;
    else if (pPalabra1 && !pPalabra2) return 1;
    else if (pPalabra1->tamano != pPalabra2->tamano) return 1;
    else {
        if (pPalabra1->tamano > pPalabra2->tamano) return 1;
        else if (pPalabra1->tamano == pPalabra2->tamano) return 0;
        else return -1;
    }
}

/**
 * Devlover la letra por una posicion dada
 * @param p
 * @param pos
 * @return 
 */
char* palabraEnPosicion(Palabra* p, int pos) {
    if (!p || pos < 0) return NULL;
    return p->cadena[pos];
}
/**
 * Comprueba si no hay letra en la palabra
 * @param p
 * @return 
 */
Bool palabra_is_empty(Palabra *p) {
    if (!p) return TRUE;
    if (p->tamano == 0) return TRUE;
    else return FALSE;
}

/**
 * 
 * @param pPalabra
 * @param pos
 * @return 
 */
Palabra * palabraCopia_desde_position(Palabra * pPalabra, int pos) {

    Palabra* pNew = NULL;

    if (!pPalabra || pos < 0 || pos > pPalabra->tamano) return NULL;

    pNew = palabraNueva();

    if (!pNew) return NULL;
    for (int i = pos; i < pPalabra->tamano; i++) {
        pNew = palabraInsertaLetra(pNew, pPalabra->cadena[i]);
    }
    return pNew;
}