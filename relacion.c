/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include "relacion.h"
#include "matrix.h"
struct _Relacion{
    char* nombre;
    Matrix* relacion_i;
    Matrix* relacion;
    Matrix* cierre_relacion;
    int num_elementos;
};
/**
 * Crea una nueva relación a la que se le da como nombre el proporcionado como argumento para un conjunto con el número de elementos que se prorpociona como argumento.
 * Debe reservar memoria propia para todas las componentes que decidas utilizar en la relación.
 * @param nombre
 * @param num_elementos
 * @return 
 */
Relacion * relacionNueva(char * nombre, int num_elementos){
    Relacion* r;
    r = (Relacion *) malloc(sizeof (Relacion));
    r->nombre = (char*) malloc(sizeof (char)*(strlen(nombre) + 1));
    strcpy(r->nombre, nombre);
    r->num_elementos = num_elementos;
    r->relacion = create_Zeromatrix(num_elementos);
    r->relacion_i = create_Zeromatrix(num_elementos);
    r->cierre_relacion = create_Zeromatrix(num_elementos);
    return r;
}
/**
 * 
 * @param fd
 * @param p_r
 */

void relacionImprime(FILE * fd, Relacion * p_r) {
    if (!p_r || !fd) return;
    printf("%s\n", p_r->nombre);
    printf("RELACION\n");
    print_matrix(fd, p_r->relacion);
    
    printf("RELACION_I\n");
    print_matrix(fd, p_r->relacion_i);
     
    printf("CIERRE\n");
    print_matrix(fd, p_r->cierre_relacion);

}

/**
 * @brief Libera toda la memoria asociada con la relación.
 * @param p_r
 */
void relacionElimina(Relacion * p_r){
    if (!p_r) return;
    free(p_r->nombre);
    destroy_matrix(p_r->cierre_relacion);
    destroy_matrix(p_r->relacion);
    destroy_matrix(p_r->relacion_i);
    free(p_r);
    
}

/**
 * Genera en memoria nueva una copia de la relación proporcionada como argumento y la devuelve.
 * @param p_r1
 * @return 
 */
Relacion * relacionCopia(Relacion * p_r1) {
    Relacion* r;
    r = relacionNueva(p_r1->nombre, p_r1->num_elementos);
    copy_value_matrix(r->relacion, p_r1->relacion);
    copy_value_matrix(r->relacion_i, p_r1->relacion_i);
    copy_value_matrix(r->cierre_relacion, p_r1->cierre_relacion);
    return r;

}

/**
 * @brief Modifica la relación proporcionada como argumento para que 
 * tenga constancia de que el elemento i está relacionado con el j.Se 
 * está suponiendo que los elementos están dispuestos en un orden 
 * precondebido y conocido por el usuario de la librería. Una vez 
 * modificada, la relación es también devuelta.
 * @param p_r
 * @param i
 * @param j
 * @return 
 */
Relacion * relacionInserta(Relacion * p_r, int row, int col) {
    if (!p_r) return NULL;
    setValue_by_position_matrix(p_r->relacion, row, col, 1);
    return p_r;
}

/**
 * @brief Devuelve el cardinal del conjunto sobre el que está definida la relación.
 * @param p_r
 * @return 
 */
int  relacionTamano(Relacion * p_r){
    if(!p_r ) return 0;
    return p_r->num_elementos;
}

/**
 * @brief Modifica la relación para conservar el ella su cierre transitivo. Devuelve la relación como retorno.
 * @param p_r
 * @return 
 */
Relacion * relacionCierreTransitivo(Relacion * p_r) {
    int multi_times;
    int exit = 1;
    if (!p_r) return NULL;
    multi_times = 2;
    /*Fetch the first matrix to matrix relacion_i*/
    copy_value_matrix(p_r->relacion_i,p_r->relacion);
    copy_value_matrix(p_r->cierre_relacion,p_r->relacion);
    
    while (exit) {
        Matrix* cierre_aux;
        Matrix* multi;
        
        
        multi = power_of_matrix(p_r->relacion, multi_times);
                
        cierre_aux = union_of_matrix(multi, p_r->cierre_relacion);
        /*
        printf("CIERRE\n");
        print_matrix(stdout, cierre_aux);
        printf("L**%d\n", multi_times);
        print_matrix(stdout, multi);
        printf("relacion_i \n");
        print_matrix(stdout, p_r->relacion_i);
        */
        
        if (is_same_value_matrix(p_r->cierre_relacion, cierre_aux) == TRUE){
            exit = 0;
        }
        
        copy_value_matrix(p_r->relacion_i, multi);
        copy_value_matrix(p_r->cierre_relacion, cierre_aux);
        
        multi_times++;
        destroy_matrix(multi);
        destroy_matrix(cierre_aux);
        
    }
    printf("\n");

    return p_r;
}

/**
 * @brief Devuelve 1 si el elemento i está relacionado originalmente con el j y 0 en caso contrario.
 * @param p_r
 * @param i
 * @param j
 * @return 
 */
int relacionIJ(Relacion * p_r, int i, int j) {
    if (!p_r) return 0;
    if (getValueByPosition_matrix(p_r->relacion, i, j) > 0)
        return 1;
    else
        return 0;
}

/**
 * Devuelve 1 si el elemento i está relacionado (en el cierre transitivo) con el j y 0 en el caso contrario.
 * @param p_r
 * @param i
 * @param j
 * @return 
 */
int relacionCierreIJ(Relacion * p_r, int i, int j) {
    if (!p_r) return 0;
    //if (getValueByPosition_matrix(p_r->cierre_relacion, i, j) > 0)
    if (getValueByPosition_matrix(p_r->relacion, i, j) > 0)
        return 1;
    else
        return 0;
}

