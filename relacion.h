/* 
 * File:   relacion.h
 * Author: MINH HAI LE
 * Created on 2 de noviembre de 2017, 16:51
 */

#ifndef RELACION_H
#define RELACION_H

#ifdef __cplusplus
extern "C" {
#endif
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
typedef struct _Relacion Relacion;
/**
 * @brief Crea una nueva relación a la que se le da como nombre el proporcionado como argumento para un conjunto con el número de elementos que se prorpociona como argumento.
 * @param nombre
 * @param num_elementos
 * @return 
 */
Relacion * relacionNueva(char * nombre, int num_elementos);
/**
 * @brief Muestra por el FILE * la relación. Puedes suponer el formato de salida utilizado en los ejemplos.
 * @param fd
 * @param p_r
 */
void relacionImprime(FILE * fd, Relacion * p_r);
/**
 * @brief Libera toda la memoria asociada con la relación.
 * @param p_r
 */
void relacionElimina(Relacion * p_r);
/**
 * @brief Genera en memoria nueva una copia de la relación proporcionada como argumento y la devuelve.
 * @param p_r1
 * @return 
 */
Relacion * relacionCopia(Relacion * p_r1);

/**
 * @brief Modifica la relación proporcionada como argumento para que tenga constancia de que el elemento i está relacionado con el j. 
 * Se está suponiendo que los elementos están dispuestos en un orden precondebido y conocido por el usuario de la librería. 
 * Una vez modificada, la relación es también devuelta.
 * @param p_r
 * @param i
 * @param j
 * @return 
 */

Relacion * relacionInserta(Relacion * p_r, int i, int j);

/**
 * @brief Devuelve el cardinal del conjunto sobre el que está definida la relación.
 * @param p_r
 * @return 
 */
int  relacionTamano(Relacion * p_r);

/**
 * @brief Modifica la relación para conservar el ella su cierre transitivo. Devuelve la relación como retorno.
 * @param p_r
 * @return 
 */
Relacion * relacionCierreTransitivo(Relacion * p_r);

/**
 * @brief Devuelve 1 si el elemento i está relacionado originalmente con el j y 0 en caso contrario.
 * @param p_r
 * @param i
 * @param j
 * @return 
 */
int  relacionIJ(Relacion * p_r,int i, int j);
/**
 * @brief Devuelve 1 si el elemento i está relacionado (en el cierre transitivo) con el j y 0 en el caso contrario.
 * @param p_r
 * @param i
 * @param j
 * @return 
 */
int relacionCierreIJ(Relacion * p_r, int i, int j);

#ifdef __cplusplus
}
#endif

#endif /* RELACION_H */

