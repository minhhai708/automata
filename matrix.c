
/* 
 * File:   matrix.c
 * Author: LE MINH HAI
 */
#include "matrix.h"
#include "basic_types.h"
/***/
struct _Matrix {
    int** table;
    int numRow;
    int numCol;
};

/**
 * 
 * @param d1
 * @param d2
 * @return 
 */
Matrix* create_Zeromatrix(int size) {
    Matrix* m;
    int i, j;
    int numRow = size;
    int numCol = size;
    m = (Matrix *) malloc(sizeof (Matrix));
    m->table = (int **) malloc(sizeof (int*) * numRow);
    for (i = 0; i < numRow; i++) {
        m->table[i] = (int *) malloc(sizeof (int)*numCol);
        for (j = 0; j < numCol; j++) {
            m->table[i][j] = 0;
        }
    }
    m->numCol = numCol;
    m->numRow = numRow;

    return m;
}

/**
 * 
 * @param matrix
 */
void print_matrix(FILE* fd, Matrix* matrix) {
    int i, j;
    printf("   ");
    for (i = 0; i < matrix->numRow; i++) {
        fprintf(fd, "[%d]", i);
    }
    printf("\n");
    for (i = 0; i < matrix->numRow; i++) {
        fprintf(fd, "[%d]", i);
        for (j = 0; j < matrix->numCol; j++) {
            fprintf(fd, " %d ", matrix->table[i][j]);
        }
        printf("\n");
    }
}

/**
 * 
 * @param matrix
 */
void destroy_matrix(Matrix* matrix) {
    int i;
    if (!matrix) return;
    for (i = 0; i < matrix->numRow; i++) {
        free(matrix->table[i]);
    }
    free(matrix->table);
    free(matrix);
}

/**
 * 
 * @param matrix
 * @param fila
 * @param col
 */
void setValue_by_position_matrix(Matrix* matrix, int fila, int col, int value) {
    if (!matrix) return;
    matrix->table[fila][col] = value;
}

/**
 * 
 * @param matrix
 * @return 
 */
Matrix* copy_matrix(Matrix* matrix) {
    Matrix* mcopy = NULL;
    int i, j;
    if (!matrix) return NULL;
    mcopy = create_Zeromatrix(matrix->numCol);
    for (i = 0; i < matrix->numRow; i++) {
        for (j = 0; j < matrix->numCol; j++) {
            mcopy->table[i][j] = matrix->table[i][j];
        }
    }
    return mcopy;
}

/**
 * C
 * @param m1
 * @param m2
 */
void copy_value_matrix(Matrix* m1, Matrix* m2) {
    int i, j;
    if (m1->numCol != m2->numCol) return;

    for (i = 0; i < m1->numRow; i++) {
        for (j = 0; j < m1->numCol; j++) {
            m1->table[i][j] = m2->table[i][j];
        }
    }
}

/**
 * 
 * @param m1
 * @param m2
 * @return 
 */
Bool is_same_value_matrix(Matrix* m1, Matrix* m2) {
    int i, j;
    if (m1->numCol != m2->numCol) return 0;
    for (i = 0; i < m1->numRow; i++) {
        for (j = 0; j < m1->numCol; j++) {
            if (m1->table[i][j] != m2->table[i][j])
                return FALSE;
        }
    }
    return TRUE;

}


/**
 * 
 * @param m
 * @param col
 * @param row
 * @return 
 */

int getValueByPosition_matrix(Matrix* m, int col, int row) {
    if (!m) return -1;
    else return m->table[row][col];
}

/**
 * 
 * @param matrix
 * @return 
 */
Matrix* power_of_matrix(Matrix* m, int p) {
    int i, j, k;
    int c;
    Matrix* resultado = create_Zeromatrix(m->numCol);
    Matrix* aux = copy_matrix(m);


    for (c = 1; c < p; c++) {

        for (i = 0; i < m->numCol; i++) {
            for (j = 0; j < m->numCol; j++) {
                int sum = 0;
                for (k = 0; k < m->numCol; k++) {
                    sum = sum + aux->table[i][k] * m->table[k][j];
                    /*printf ("t[%d][%d](%d)*t[%d][%d](%d) + " ,i,k,aux->table[i][k],
                            k,j,m->table[k][j]);*/
                }
                /*resultado->table[i][j] = sum;*/
                if (sum == 0)
                    resultado->table[i][j] = 0;
                else 
                    resultado->table[i][j] = 1;
                 
            }
        }
        copy_value_matrix(aux, resultado);
    }
    destroy_matrix(aux);
    return resultado;
}

Matrix* union_of_matrix(Matrix* m1, Matrix* m2) {
    Matrix* aux;
    int i, j;
    aux = create_Zeromatrix(m1->numCol);

    for (i = 0; i < m1->numCol; i++) {
        for (j = 0; j < m1->numCol; j++) {
            if ((m1->table[i][j] + m2->table[i][j]) != 0) {
                aux->table[i][j] = 1;
            }
        }
    }
    return aux;
}