#ifndef _TRANSICION_AP_H
#define _TRANSICION_AP_H

#include <stdio.h>
#include "palabra.h"
#include "list.h"

typedef struct _TransicionAP TransicionAP;
/**
 * 
 * @param nombre
 * @param num_simbolos_pila
 * @param num_estados
 * @param num_simbolos_entrada
 * @param nombres_pila
 * @param nombres_estados
 * @param nombres_entrada
 * @return 
 */
TransicionAP * transicionAPNueva(char * nombre, int num_simbolos_pila, int num_estados, int num_simbolos_entrada, List * nombres_pila, List * nombres_estados, List * nombres_entrada);
/**
 * 
 * @param fd
 * @param p_t
 */
void transicionAPImprime(FILE * fd, TransicionAP * p_t);
/**
 * 
 * @param p_t
 */
void transicionAPElimina(TransicionAP * p_t);
/**
 * Insertar una accion en matrix de accion
 * @param p_t
 * @param simbolo_pila
 * @param estado_i
 * @param estado_f
 * @param simbolo_entrada
 * @param accion
 * @return 
 */
TransicionAP * transicionAPInsertaAccion(TransicionAP * p_t, int simbolo_pila, int estado_i, int estado_f, int simbolo_entrada, Palabra * accion);
/**
 * Delvoler lista de las acciones em matrix de transicion
 * @param p_t
 * @param i
 * @param j
 * @param k
 * @param l
 * @return 
 */
List * transicionAPAccionesIJKL(TransicionAP * p_t, int i, int j, int k, int l);




#endif
