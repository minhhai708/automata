

/* 
 * File:   main.c
 * Author: temporal
 *
 * Created on 5 de octubre de 2017, 17:08
 */

#include <stdio.h>
#include <stdlib.h>
#include "stack.h"
#include "palabra.h"
#include "estado.h"
#include "configuracion_ap.h"
#include "configuracion_apnd.h"
#include "matrix.h"
#include "relacion.h"
#include "alfabeto.h"
#include "ap.h"
#include "transicion_ap.h"

void test_AP() {
    AP * ap;
    Palabra * accion;


    /* CREACIÓN DEL AUTÓMATA */

    /*ESTADOS ENTRADA PILA */
    ap = APNuevo("enunciado", 3, 3, 2);


    /* SE INSERTAN ALFABETOS DE ENTRADA, PILA Y ESTADOS */

    APInsertaSimboloAlfabetoEntrada(ap, "a");
    APInsertaSimboloAlfabetoEntrada(ap, "b");
    APInsertaSimboloAlfabetoEntrada(ap, ENTRADA_LAMDA);
    
    APInsertaSimboloAlfabetoPila(ap, "a");
    APInsertaSimboloAlfabetoPila(ap, SIMBOLO_INICIO_PILA);
    APInsertaEstado(ap, "q0", INICIAL);
    APInsertaEstado(ap, "q1", NORMAL);
    APInsertaEstado(ap, "q2", FINAL);

    /* SE INSERTAN TRANSICIONES QUE MODIFICAN ENTRADA O PILA*/

    /* cima pila:Z estadoi: q0 estadof: q0 entrada: a pila: aZ */
    accion = palabraNueva();
    palabraInsertaLetra(accion, "a");
    palabraInsertaLetra(accion, SIMBOLO_INICIO_PILA);
    APInsertaTransicion(ap, SIMBOLO_INICIO_PILA, "q0", "q0", "a", accion);
    palabraElimina(accion);




    /* cima pila:Z estadoi: q0 estadof: q0 entrada: a pila: aaZ */
    accion = palabraNueva();
    palabraInsertaLetra(accion, "a");
    palabraInsertaLetra(accion, "a");
    palabraInsertaLetra(accion, SIMBOLO_INICIO_PILA);
    APInsertaTransicion(ap, SIMBOLO_INICIO_PILA, "q0", "q0", "a", accion);
    palabraElimina(accion);




    /* cima pila:Z estadoi: q0 estadof: q0 entrada: a pila: aaaZ */
    accion = palabraNueva();
    palabraInsertaLetra(accion, "a");
    palabraInsertaLetra(accion, "a");
    palabraInsertaLetra(accion, "a");
    palabraInsertaLetra(accion, SIMBOLO_INICIO_PILA);
    APInsertaTransicion(ap, SIMBOLO_INICIO_PILA, "q0", "q0", "a", accion);
    palabraElimina(accion);




    /* cima pila:a estadoi: q0 estadof: q0 entrada: a pila: aa */
    accion = palabraNueva();
    palabraInsertaLetra(accion, "a");
    palabraInsertaLetra(accion, "a");
    APInsertaTransicion(ap, "a", "q0", "q0", "a", accion);
    palabraElimina(accion);




    /* cima pila:a estadoi: q0 estadof: q0 entrada: a pila: aaa */
    accion = palabraNueva();
    palabraInsertaLetra(accion, "a");
    palabraInsertaLetra(accion, "a");
    palabraInsertaLetra(accion, "a");
    APInsertaTransicion(ap, "a", "q0", "q0", "a", accion);
    palabraElimina(accion);




    /* cima pila:a estadoi: q0 estadof: q0 entrada: a pila: aaaa */
    accion = palabraNueva();
    palabraInsertaLetra(accion, "a");
    palabraInsertaLetra(accion, "a");
    palabraInsertaLetra(accion, "a");
    palabraInsertaLetra(accion, "a");
    APInsertaTransicion(ap, "a", "q0", "q0", "a", accion);
    palabraElimina(accion);

    /* cima pila:a estadoi: q0 estadof: q1 entrada: b  pila: lambda (como NULL) */
    APInsertaTransicion(ap, "a", "q0", "q1", "b", NULL);



    /* cima pila:a estadoi: q1 estadof: q1 entrada: b  pila: lambda (como NULL) */
    APInsertaTransicion(ap, "a", "q1", "q1", "b", NULL);


    /* cima pila:lambda Z  estadoi: q1 estadof: q2 entrada: lmabda (como NULL)  pila: lambda (como NULL) */
    APInsertaTransicion(ap, "Z", "q1", "q2", NULL, NULL);

    /* SE INSERTAN TRANSICIONES LAMBDA PURAS */

    /* HAY QUE CERRAR LAS TRANSICIONES LAMBDA (NO HAY PERO HAY QUE HACERLO EN GENERAL) */
    APCierraLTransicion(ap);

    /* SE INSETA LA PALABRA */


    APInsertaLetra(ap, "a");
    APInsertaLetra(ap, "b");
    APInsertaLetra(ap, "b");
    APInsertaLetra(ap, "b");
    APInsertaLetra(ap, "b");

    /* HAY QUE INICIALIZAR EL ESTADO DEL AUTÓMATA PARA EMPEZAR A PROCESAR */

    APInicializaEstado(ap);
    APImprime(stdout, ap);
    
    APProcesaEntrada(stdout, ap);
    
    APElimina(ap);
}

void test_transicionAP() {
    TransicionAP* tap;
    List * pila;
    List * estados;
    List * entradas;
    Palabra * accion;
 
    accion = palabraNueva();
    palabraInsertaLetra(accion, "a");
    palabraInsertaLetra(accion, SIMBOLO_INICIO_PILA);

    pila = list_ini((destroy_element_function_type) palabraElimina,
            (copy_element_function_type) palabraCopia,
            (print_element_function_type) palabraImprime,
            (cmp_element_function_type) palabraCompara
            );
    estados = list_ini((destroy_element_function_type) palabraElimina,
            (copy_element_function_type) palabraCopia,
            (print_element_function_type) palabraImprime,
            (cmp_element_function_type) palabraCompara
            );
    entradas = list_ini((destroy_element_function_type) palabraElimina,
            (copy_element_function_type) palabraCopia,
            (print_element_function_type) palabraImprime,
            (cmp_element_function_type) palabraCompara
            );
    tap = transicionAPNueva("Transaccion", 3, 2, 2, pila, estados, entradas);

    transicionAPInsertaAccion(tap, 1, 1, 1, 1, accion); 

    transicionAPImprime(stdout, tap);
    
    list_destroy(pila);
    list_destroy(estados);
    list_destroy(entradas);

    palabraElimina(accion);
    
    list_print(stdout, transicionAPAccionesIJKL(tap, 1,1,1,1));
    
    transicionAPElimina(tap);
    
}

void test_stack() {
    Stack* stack = NULL;
    Stack* stack2 = NULL;
    Stack* stack3 = NULL;
    char* ele = NULL;

    stack = stack_ini((destroy_element_function_type) destroy_p_string,
            (copy_element_function_type) copy_p_string,
            (print_element_function_type) print_p_string,
            (cmp_element_function_type) cmp_p_string);

    stack2 = stack_ini((destroy_element_function_type) destroy_p_string,
            (copy_element_function_type) copy_p_string,
            (print_element_function_type) print_p_string,
            (cmp_element_function_type) cmp_p_string);
    stack_push(stack, "1-Hello world\n");
    stack_push(stack, "2-HALO world\n");
    stack_push(stack, "3-HALO world\n");
    stack_print(stdout, stack);
    while (!stack_isEmpty(stack)) {
        ele = stack_pop(stack);
        stack_push(stack2, ele);
        destroy_p_string(ele);

    }
    stack3 = stack_copy(stack2);
    
    if (stack_compare(stack2, stack3) == 0){
        printf("SAME stack");
    }
    
    stack_print(stdout, stack3);
    stack_destroy(stack3);
    stack_destroy(stack2);
    stack_destroy(stack);
}

void test_palabra() {

    Palabra * cadena;
    Palabra * aux;
    cadena = palabraNueva();

    palabraInsertaLetra(cadena, "a1");
    palabraInsertaLetra(cadena, "a2");
    palabraInsertaLetra(cadena, "a3");
    palabraInsertaLetra(cadena, "a4");


    palabraImprime(stdout, cadena);
    aux = palabraCopia(cadena);
    printf("cadena\n");
    palabraImprime(stdout, aux);
    printf("cadena COPIA\n");
    palabraImprime(stdout, cadena);

    palabraElimina(aux);
    palabraElimina(cadena);


}

void test_estado() {
    Estado * estado = NULL;
    Estado * estado2 = NULL;
    Estado * estado3 = NULL;
    estado = estadoNuevo("q1", 2);
    estadoImprime(stdout, estado);

    estado2 = estado_copy(estado);
    estado3 = estado_copy(estado2);
    estadoImprime(stdout, estado);
    estadoImprime(stdout, estado2);
    estadoImprime(stdout, estado3);

    if (estadoEs(estado2, "q1") == 1) {
        printf("IGUAL\n");
    }
    estadoElimina(estado3);
    estadoElimina(estado2);
    estadoElimina(estado);

    return;
}

void test_configuracionAp() {
    ConfiguracionAp * p_cap;
    ConfiguracionAp * p_cap2;

    char texto[1024];

    Stack * pila;
    Estado * estado;
    Palabra * cadena;

    cadena = palabraNueva();

    pila = stack_ini((destroy_element_function_type) destroy_p_string,
            (copy_element_function_type) copy_p_string,
            (print_element_function_type) print_p_string,
            (cmp_element_function_type) strcmp);

    estado = estadoNuevo("q1", 2);

    palabraInsertaLetra(cadena, "a1");
    palabraInsertaLetra(cadena, "a2");
    palabraInsertaLetra(cadena, "a3");
    palabraInsertaLetra(cadena, "a4");

    sprintf(texto, "z");
    stack_push(pila, texto);
    sprintf(texto, "a");
    stack_push(pila, texto);
    sprintf(texto, "b");
    stack_push(pila, texto);
   
    p_cap = configuracionApNueva(estado, pila, cadena);
    p_cap2 = configuracionApCopia(p_cap);

    fprintf(stdout, "\nCONFIGURACION 1\n");
    configuracionApImprime(stdout, p_cap);
    fprintf(stdout, "\nCONFIGURACION 2\n");
    configuracionApImprime(stdout, p_cap2);

    
    stack_destroy(pila);
    palabraElimina(cadena);
    estadoElimina(estado);
    
    configuracionApElimina(p_cap);
    configuracionApElimina(p_cap2);
    
 
}

int test_configuracionApnd() {

    ConfiguracionAp * p_cap;
    ConfiguracionAp * p_cap2;
    ConfiguracionAp * p_cap_aux;
    char texto[1024];

    ConfiguracionApnd * capnd;

    Stack * pila;
    Estado * estado;
    Palabra * cadena;
    
    cadena = palabraNueva();
    capnd = configuracionApndIni();
    
    palabraInsertaLetra(cadena, "a1");
    palabraInsertaLetra(cadena, "a2");
    palabraInsertaLetra(cadena, "a3");
    palabraInsertaLetra(cadena, "a4");

    pila = stack_ini((destroy_element_function_type) destroy_p_string,
            (copy_element_function_type) copy_p_string,
            (print_element_function_type) print_p_string,
            (cmp_element_function_type) strcmp);

    sprintf(texto, "z");
    stack_push(pila, texto);
    sprintf(texto, "a");
    stack_push(pila, texto);
    sprintf(texto, "b");
    stack_push(pila, texto);
    estado = estadoNuevo("q1", 2);

    p_cap = configuracionApNueva(estado, pila, cadena);
    p_cap2 = configuracionApCopia(p_cap);
 
    fprintf(stdout, "\nCONFIGURACION 1\n");
    configuracionApImprime(stdout, p_cap);

    fprintf(stdout, "\nCONFIGURACION 2\n");
    configuracionApImprime(stdout, p_cap2);  
  
    configuracionApndInsert(capnd, p_cap);   
    configuracionApndInsert(capnd, p_cap2);

    
    configuracionApElimina(p_cap);
    configuracionApElimina(p_cap2);
    
    estadoElimina(estado);
    stack_destroy(pila);
    palabraElimina(cadena);


    fprintf(stdout, "\nCONFIGURACION NO DETERMINISTA\n");
    

    fprintf(stdout, "\nCONFIGURACION NO DETERMINISTA, IMPRESA ELEMENTO A ELEMENTO\n");
    while (configuracionApndIsEmpty(capnd) != 1) {
        p_cap_aux = configuracionApndExtract(capnd);
        fprintf(stdout, "\nELEMENTO\t");
        configuracionApImprime(stdout, p_cap_aux);
        configuracionApElimina(p_cap_aux);
        
    }
    
    configuracionApndPrint(stdout, capnd);
    
    configuracionApndDestroy(capnd);
    
    
    return 0;
}

void test_matrix() {
    Matrix * matrix;
    Matrix * resultado;
    int size = 3;
    matrix = create_Zeromatrix(size);
    setValue_by_position_matrix(matrix, 2, 1, 1);
    setValue_by_position_matrix(matrix, 1, 2, 1);
    setValue_by_position_matrix(matrix, 2, 2, 1);
    print_matrix(stdout, matrix);
    resultado = (Matrix*) power_of_matrix(matrix, 4);
    print_matrix(stdout, resultado);

    destroy_matrix(matrix);
    destroy_matrix(resultado);

}

int test_relacion() {
    Relacion * r1;
    Relacion * r2;
    r1 = relacionNueva("grafo1", 8);
    relacionInserta(r1, 0, 1);
    relacionInserta(r1, 0, 2);
    relacionInserta(r1, 1, 4);
    relacionInserta(r1, 2, 3);
    relacionInserta(r1, 3, 0);
    relacionInserta(r1, 4, 6);
    relacionInserta(r1, 5, 4);
    relacionInserta(r1, 5, 7);
    relacionInserta(r1, 6, 5);
    fprintf(stdout, "R1\n");
    relacionImprime(stdout, r1);
    r2 = relacionCopia(r1);

    fprintf(stdout, "R2 = copia(R1)\n");
    fprintf(stdout, "R2\n");
    relacionImprime(stdout, r2);

    fprintf(stdout, "R1*\n");
    relacionCierreTransitivo(r1);
    relacionImprime(stdout, r1);

    relacionElimina(r1);
    relacionElimina(r2);

    return 0;

}

void test_alfabeto() {
    Alfabeto * alf;
    alf = alfabetoNuevo("EPS", 3);
    
    alfabetoInsertaSimbolo(alf, "a");
    alfabetoInsertaSimbolo(alf, "b");
    alfabetoInsertaSimbolo(alf, "c");
    
    alfabetoImprime(stdout, alf);
    
    printf("Simbolo en posicion %d es %s\n", 1, alfabetoSimboloEn(alf, 1));
    printf("Simbolo %s en posicion %d\n", "b", alfabetoIndiceDeSimbolo(alf, "b"));
    alfabetoElimina(alf);
}

int main(int argc, char** argv) {

    test_alfabeto();
    test_matrix();
    test_relacion();
    test_palabra();
    test_stack();
    test_estado();
    test_configuracionAp();
    test_transicionAP();
    test_configuracionApnd();
    test_AP();
    return 1;
}
