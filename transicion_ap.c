#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "transicion_ap.h"
#include "list.h"
#include "palabra.h"
#include "generic_collections_types.h"

struct _TransicionAP {
    char * nombre;
    List * **** acciones;
    int num_simbolos_pila;
    int num_estados;
    int num_simbolos_entrada;
    List * nombres_pila;
    List * nombres_estados;
    List * nombres_entrada;
};

TransicionAP * transicionAPNueva(char * nombre, int num_simbolos_pila, int num_estados, int num_simbolos_entrada, 
        List * nombres_pila, List * nombres_estados, List * nombres_entrada) {
    TransicionAP* tap;
    tap = (TransicionAP*) malloc(sizeof (TransicionAP));

    tap->nombre = (char*) malloc(sizeof (char)*(strlen(nombre) + 1));
    strcpy(tap->nombre, nombre);

    tap->num_estados = num_estados;
    tap->num_simbolos_entrada = num_simbolos_entrada;
    tap->num_simbolos_pila = num_simbolos_pila;

    tap->nombres_entrada = nombres_entrada;
    tap->nombres_pila = nombres_pila;
    tap->nombres_estados = nombres_estados;

    tap->acciones = (List* ****) malloc(num_simbolos_pila * sizeof (List* ***));
    
    for (int i = 0; i < num_simbolos_pila; i++) {
        tap->acciones[i] = (List* ***) malloc(num_estados * sizeof (List* **));
        for (int j = 0; j < num_estados; j++) {
            tap->acciones[i][j] = (List* **) malloc(num_estados * sizeof (List* *));
            for (int k = 0; k < num_estados; k++) {
                tap->acciones[i][j][k] = (List* *) malloc(num_simbolos_entrada * sizeof (List*));
                for (int l = 0; l < num_simbolos_entrada; l++) {
                    
                    tap->acciones[i][j][k][l] = list_ini((destroy_element_function_type) palabraElimina,
                            (copy_element_function_type) palabraCopia,
                            (print_element_function_type) palabraImprime,
                            (cmp_element_function_type) palabraCompara
                            );
                }
            }
        }
    }
    return tap;
}
/**
 * Print all transition in output
 * @param fd
 * @param tap
 */
void transicionAPImprime(FILE * fd, TransicionAP * tap) {
    
    int num_simbolos_pila = tap->num_simbolos_pila;
    int num_estados = tap->num_estados;
    int num_simbolos_entrada = tap->num_simbolos_entrada;
    
    if (!fd || !tap) return;
    fprintf(fd, "Transicion: %s\n", tap->nombre);

    for (int i = 0; i < num_simbolos_pila; i++) {
        for (int j = 0; j < num_estados; j++) {
            for (int k = 0; k < num_estados; k++) {
                for (int l = 0; l < num_simbolos_entrada; l++) {
                    if (list_isEmpty(tap->acciones[i][j][k][l]) == FALSE) {
                        list_print(fd, tap->acciones[i][j][k][l]);
                        printf("%d %d %d %d\n", i, j, k, l);
                    }
                }
            }
        }
    }
}

void transicionAPElimina(TransicionAP * tap) {
    if (!tap) return;
    int num_simbolos_pila = tap->num_simbolos_pila;
    int num_estados = tap->num_estados;
    int num_simbolos_entrada = tap->num_simbolos_entrada;
    for (int i = 0; i < num_simbolos_pila; i++) {
        for (int j = 0; j < num_estados; j++) {
            for (int k = 0; k < num_estados; k++) {
                for (int l = 0; l < num_simbolos_entrada; l++) {
                    list_destroy(tap->acciones[i][j][k][l]);
                }
                free(tap->acciones[i][j][k]);
            }
            free(tap->acciones[i][j]);
        }
        free(tap->acciones[i]);
    }
    free(tap->acciones);
    free(tap->nombre);
    free(tap);
}

TransicionAP * transicionAPInsertaAccion(TransicionAP * tap,
        int simbolo_pila, int estado_i, int estado_f, int simbolo_entrada, Palabra * accion) {
    
    int i = simbolo_pila;
    int j = estado_i;
    int k = estado_f;
    int l = simbolo_entrada;
    if(accion != NULL)
        list_insertFirst(tap->acciones[i][j][k][l], accion);
    else{
        Palabra* palabra = palabraNueva();
        list_insertFirst(tap->acciones[i][j][k][l], palabra);
        palabraElimina(palabra);
    }
    
    return tap;
}

List * transicionAPAccionesIJKL(TransicionAP * tap, int i, int j, int k, int l) {
    if (!tap) return NULL;
    if (i >= tap->num_simbolos_pila ||
            j >= tap->num_estados ||
            k >= tap->num_estados ||
            l >= tap->num_simbolos_entrada)return NULL;
    return tap->acciones[i][j][k][l];
}


