/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include "generic_string.h"
/**
 * 
 * @param file
 * @param ele
 * @return 
 */
int print_p_string(FILE* file, char* ele){
    int sizeEle = 0;
    if (!file || !ele) return 0;
    
    fprintf(file,"%s ",ele);
    sizeEle = strlen(ele);
    return sizeEle;
}
/**
 * 
 * @param ele
 */
void destroy_p_string(char* ele){
    if (!ele) return;
   /* printf("DESTROY ELE\n");*/
    free(ele);
}

/**
 * 
 * @param ele
 * @return 
 */
char* copy_p_string(char* ele) {
    char* str = NULL;
    
    str = (char *) malloc((strlen(ele)+1) * sizeof(char));    
    if (!str) 
        return NULL;    
    memcpy(str,ele, strlen(ele)+1);  
    return str;
}
/**
 * 
 * @param ele1
 * @param ele2
 * @return 
 */

int cmp_p_string(char* ele1, char* ele2) {
    return strcmp(ele1, ele2);
    /*    if(!ele1 && !ele2) return 0;
        else if( !ele1 && ele2 ) return -1;
        else if(ele1 && !ele2) return 1;
        else{
            return strcmp(ele1, ele2);
        }
     */
}
