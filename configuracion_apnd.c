/*
 * @author LE MINH HAI
 * @date 31/10/2017
 * @filename configuracion_apnd.c
 */

#include "configuracion_apnd.h"

/**
 * @brief Inicializa una configuración no determinista
 * @param destroy
 * @param copy
 * @param print
 * @param compare
 * @return 
 */
ConfiguracionApnd * configuracionApndIni() {
    ConfiguracionApnd* capnd = NULL;
    capnd= (ConfiguracionApnd *) list_ini((destroy_element_function_type) configuracionApElimina,
            (copy_element_function_type) configuracionApCopia,
            (print_element_function_type) configuracionApImprime,
            (cmp_element_function_type) configuracionCompara);
    return capnd;
}

/**
 * @brief Inserta una configuración determinista en una no determinista. 
 * Se realiza una copia en memoria propia de la colección para el nuevo elemento y se asegura que no haya duplicados
 * @param capnd
 * @param p_cap
 * @return 
 */
ConfiguracionApnd* configuracionApndInsert(ConfiguracionApnd* capnd, const ConfiguracionAp* p_cap) {
    ConfiguracionAp * p_cap_aux;
    if (!capnd || !p_cap) return NULL;
    
    if (list_belongs((List*) capnd, (ConfiguracionAp*) p_cap) == 1) return capnd;

    p_cap_aux = configuracionApCopia((ConfiguracionAp*) p_cap);
    list_insertFirst(capnd, (ConfiguracionAp*) p_cap_aux);
    configuracionApElimina(p_cap_aux);
    return capnd;
}

/**
 * @brief  Se obtiene una configuración determinista de la no determinista, que desaparece de la colección.
 * No se especifica el orden en el que se extrae
 * @param capnd
 * @return 
 */
ConfiguracionAp * configuracionApndExtract(ConfiguracionApnd * capnd) {
    ConfiguracionAp *aux = NULL;
    if (!capnd) return NULL;
    aux = list_extractFirst(capnd);
    return aux;

}
/**
 * @brief  Se devuelve 1 si está vacía y 0 en caso contrario
 * @param capnd
 * @return 
 */
int configuracionApndIsEmpty(const ConfiguracionApnd* capnd){
    if(!capnd) return 1;
    return list_isEmpty(capnd);
}
/**
 * @brief  Se devuelve el número de configuraciones deterministas que hay dentro de la no determinista

 * @param capnd
 * @return 
 */
int configuracionApndSize(const ConfiguracionApnd * capnd){
    if(!capnd) return 0;
    return list_size(capnd);
}
/**
 * @brief  Se imprime todas las configuraciones deterministas. No se especifica en qué orden
 * @param fd
 * @param capnd
 * @return 
 */
int configuracionApndPrint(FILE *fd, const ConfiguracionApnd* capnd){
    if(!capnd || !fd) return 0;
    return list_print(fd, capnd);
}
/**
 * @brief Se libera toda la memoria asociada con la configuracion no determinista 
 * @param capnd
 */
void configuracionApndDestroy( ConfiguracionApnd* capnd){
    if(!capnd) return;
    list_destroy(capnd);
}
