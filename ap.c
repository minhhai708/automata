#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "ap.h"
#include "alfabeto.h"
#include "estado.h"
#include "palabra.h"
#include "relacion.h"
#include "configuracion_apnd.h"
#include "transicion_ap.h"
#include "list.h"
#include "generic_string.h"

struct _AP {
    char * nombre;
    Alfabeto * alfabeto_entrada;
    Alfabeto * alfabeto_pila;
    int num_estados;

    Estado ** estados;
    ConfiguracionApnd * configuracion_actual;
    Palabra * cadena_inicial;

    Relacion * transiciones_lambda_puras;
    TransicionAP* transiciones;

    List * nombres_estados;
    List * nombres_simbolos_pila;
    List * nombres_simbolos_entrada;
};

/**
 * @brief void VectorIndicesImprime(FILE *fd, VectorIndices vi, int tamano);
 * int VectorIndicesVacio(VectorIndices vi, int tamano);
 * typedef struct _AP AP;
 * @param nombre
 * @param num_estados
 * @param num_simbolos_entrada
 * @param num_simbolos_pila
 * @return 
 */
AP * APNuevo(char * nombre, int num_estados, int num_simbolos_entrada, int num_simbolos_pila) {
    AP* ap;
    ap = (AP*) malloc(sizeof (AP));
    ap->nombre = (char*) malloc(sizeof (char)*(strlen(nombre) + 1));
    strcpy(ap->nombre, nombre);

    ap->num_estados = num_estados;
    ap->alfabeto_entrada = alfabetoNuevo("SIGMA", num_simbolos_entrada);
    ap->alfabeto_pila = alfabetoNuevo("GAMMA", num_simbolos_pila);

    ap->estados = (Estado **) malloc(sizeof (Estado*) * num_estados);
    for (int i = 0; i < num_estados; i++) {
        ap->estados[i] = NULL;
    }

    ap->cadena_inicial = palabraNueva();

    ap->configuracion_actual = configuracionApndIni();
    ap->transiciones_lambda_puras = relacionNueva("Lamda Pura", num_estados);
    ap->transiciones = transicionAPNueva("Transaccion", num_simbolos_pila, num_estados, num_simbolos_entrada,
            ap->nombres_simbolos_pila, ap->nombres_estados, ap->nombres_simbolos_entrada);


    ap->nombres_estados = list_ini((destroy_element_function_type) destroy_p_string,
            (copy_element_function_type) copy_p_string,
            (print_element_function_type) print_p_string,
            (cmp_element_function_type) cmp_p_string);
    ap->nombres_simbolos_pila = list_ini((destroy_element_function_type) destroy_p_string,
            (copy_element_function_type) copy_p_string,
            (print_element_function_type) print_p_string,
            (cmp_element_function_type) cmp_p_string);

    ap->nombres_simbolos_entrada = list_ini((destroy_element_function_type) destroy_p_string,
            (copy_element_function_type) copy_p_string,
            (print_element_function_type) print_p_string,
            (cmp_element_function_type) cmp_p_string);

    return ap;
}

void APElimina(AP * ap) {

    list_destroy(ap->nombres_estados);
    list_destroy(ap->nombres_simbolos_entrada);
    list_destroy(ap->nombres_simbolos_pila);

    if (ap->transiciones) transicionAPElimina(ap->transiciones);
    if (ap->transiciones_lambda_puras) relacionElimina(ap->transiciones_lambda_puras);
    if (ap->configuracion_actual) configuracionApndDestroy(ap->configuracion_actual);
    if (ap->cadena_inicial) palabraElimina(ap->cadena_inicial);

    if (ap->estados) {
        for (int i = 0; i < ap->num_estados; i++) {
            if (ap->estados[i])
                estadoElimina(ap->estados[i]);
        }
    }
    free(ap->estados);

    if (ap->alfabeto_entrada) alfabetoElimina(ap->alfabeto_entrada);
    if (ap->alfabeto_pila) alfabetoElimina(ap->alfabeto_pila);

    free(ap->nombre);
    free(ap);
}


void APImprime(FILE * fd, AP* ap) {
    if (!fd || !ap) return;
    fprintf(fd, "AP: \n");
    relacionImprime(fd, ap->transiciones_lambda_puras);

    fprintf(fd, "Alfabeto: \n");
    alfabetoImprime(fd, ap->alfabeto_entrada);
    alfabetoImprime(fd, ap->alfabeto_pila);
    fprintf(fd, "Estado \n");
    for (int i = 0; i < ap->num_estados; i++) {
        if (ap->estados[i] != NULL)
            estadoImprime(fd, ap->estados[i]);
    }

   /* fprintf(fd, "Transicion \n");
    transicionAPImprime(fd, ap->transiciones);
*/
    fprintf(fd, "\nCadena Inicial\n");
    palabraImprime(fd, ap->cadena_inicial);
    printf("\n");
    configuracionApndPrint(fd, ap->configuracion_actual);
}

/*
 * Inserta en el alfabeto de entrada del autómata proporcionado como primer argumento un nuevo símbolo de entrada cuyo nombre se proporciona como segundo argumento.
 * Debe hacerse una copia en memoria nueva para ser guardada en el autómata.
 */
AP * APInsertaSimboloAlfabetoEntrada(AP * ap, char * simbolo) {
    if (!ap) return NULL;
    alfabetoInsertaSimbolo(ap->alfabeto_entrada, simbolo);
    return ap;
}

/*
 
 * Inserta en el alfabeto de la pila del autómata proporcionado como primer argumento 
 * un nuevo símbolo de entrada cuyo nombre se proporciona como segundo argumento.
 * Debe hacerse una copia en memoria nueva para ser guardada en el autómata.
 */
AP * APInsertaSimboloAlfabetoPila(AP * ap, char * simbolo) {
    if (!ap) return NULL;
    alfabetoInsertaSimbolo(ap->alfabeto_pila, simbolo);
    return ap;
}

/*
 * Inserta en el conjunto de estados del autómata proporcionado como primer 
 * argumento un nuevo estado cuyo nombre se proporciona como segundo argumento y del tipo proporcionado como tercer parámetro.
 * Se están utilizando los siguientes tipos (que deben ser definidos respetando estos nombres)
 * INICIAL
 * FINAL
 * NORMAL
 * INICIAL_Y_FINAL
 * Debe hacerse una copia en memoria nueva para ser guardada en el autómata.
 */

AP * APInsertaEstado(AP * ap, char * nombre, int tipo) {
    for (int i = 0; i < ap->num_estados; i++) {
        /*Check if estado has been added*/
        if (ap->estados[i] != NULL) {
            if (strcmp(estadoNombre(ap->estados[i]), nombre) == 0)
                return ap;
        }
        if (ap->estados[i] == NULL) {
            ap->estados[i] = estadoNuevo(nombre, tipo);
            return ap;
        }
    }
    return ap;
}

char * APNombreEstadoEn(AP * ap, int pos) {
    Estado * e;
    if (!ap) return NULL;

    e = ap->estados[pos];

    if (e == NULL) return NULL;
    return estadoNombre(e);
}

char * APSimboloEntradaEn(AP * ap, int pos) {
    return alfabetoSimboloEn(ap->alfabeto_entrada, pos);
}

char * APSimboloPilaEn(AP * ap, int pos) {
    return alfabetoSimboloEn(ap->alfabeto_pila, pos);
}

int APIndiceDeSimboloEntrada(AP * ap, char * nombre) {
    return alfabetoIndiceDeSimbolo(ap->alfabeto_entrada, nombre);
}

int APIndiceDeSimboloPila(AP * ap, char * nombre) {
    return alfabetoIndiceDeSimbolo(ap->alfabeto_pila, nombre);
}

int APIndiceDeEstado(AP * ap, char * nombre) {
    int i;
    for (i = 0; i < ap->num_estados; i++) {
        if (ap->estados[i] != NULL)
            if (strcmp(estadoNombre(ap->estados[i]), nombre) == 0) {
                return i;
            }
    }
    return -1;
}

void APImprimeConfiguracionApndActual(FILE * fd, AP * ap) {
    if (!ap) return;
    configuracionApndPrint(fd, ap->configuracion_actual);
}

void APImprimeCadenaActual(FILE *fd, AP * ap) {
    if (!fd || !ap) return;
    palabraImprime(fd, ap->cadena_inicial);
}

Estado * APEstadoEn(AP * ap, int pos) {
    if (!ap) return NULL;
    if (pos >= ap->num_estados) return NULL;

    return ap->estados[pos];
}

Estado * APEstadoDeNombre(AP *ap, char * nombre) {
    if (!ap || !nombre) return NULL;
    for (int i = 0; i < ap->num_estados; i++) {
        if (strcmp(estadoNombre(ap->estados[i]), nombre) == 0)
            return ap->estados[i];
    }
    return NULL;
}

/*
 * Inserta una letra nueva en la cadena de entrada que tiene que procesar el autómata.
 * Debe hacer una copia en memoria nueva de la letra si lo necesita para guardarla.
 */
AP * APInsertaLetra(AP * ap, char * letra) {
    if (!palabraInsertaLetra(ap->cadena_inicial, letra)) return NULL;
    return ap;
}

int APCierreLTransicionIJ(AP * ap, int i, int j) {
    return relacionCierreIJ(ap->transiciones_lambda_puras, i, j);
}

/**
 * Modifica el autómata proporcionado como primer argumento para realizar el cierre transitivo de las transiciones lambda puras.
 */
AP * APCierraLTransicion(AP * ap) {
    if (!relacionCierreTransitivo(ap->transiciones_lambda_puras)) return NULL;
    return ap;
}

AP * APInicializaCadena(AP * ap) {
    if (!ap || !ap->cadena_inicial) return ap;
    palabraElimina(ap->cadena_inicial);
    ap->cadena_inicial = palabraNueva();
    return ap;
}

int APIndiceEstadoInicial(AP * ap) {
    for (int i = 0; i < ap->num_estados; i++) {
        if (estadoTipo(ap->estados[i]) == INICIAL)
            return i;
        if (estadoTipo(ap->estados[i]) == INICIAL_Y_FINAL)
            return i;
    }
    return -1;
}

/**
 * Establece como configuración inicial del autómata aquella que contiene
 * el estado  que se haya declarado como estado inicial.
 * la palabra completa de entrada.
 * la pila con el símbolo de inicio de pila (“Z”)
 * y todas aquellas que comparten cadena y pila pero cuyo estado es uno al que se puede llegar mediante 𝚲+.
 */
AP * APInicializaEstado(AP * ap) {
    ConfiguracionAp* configAp;
    Stack* pila;
    Estado* estado;
    Palabra* cadena;
    /*
        if (configuracionApndIsEmpty(ap->configuracion_actual) != TRUE) {
            configuracionApndDestroy(ap->configuracion_actual);
            ap->configuracion_actual = configuracionApndIni();
        }
     */
    pila = stack_ini((destroy_element_function_type) destroy_p_string,
            (copy_element_function_type) copy_p_string,
            (print_element_function_type) print_p_string,
            (cmp_element_function_type) cmp_p_string);
    stack_push(pila, SIMBOLO_INICIO_PILA);

    cadena = ap->cadena_inicial;
    estado = APEstadoEn(ap, APIndiceEstadoInicial(ap));

    configAp = configuracionApNueva(estado, pila, cadena);

    configuracionApndInsert(ap->configuracion_actual, configAp);

    configuracionApElimina(configAp);

    for (int i = 0; i < ap->num_estados; i++) {
        if (APCierreLTransicionIJ(ap, APIndiceEstadoInicial(ap), i)) {
            ConfiguracionAp* di = configuracionApNueva(APEstadoEn(ap, i), pila, ap->cadena_inicial);
            configuracionApndInsert(ap->configuracion_actual, di);
            configuracionApElimina(di);
        }
    }

    stack_destroy(pila);
    return NULL;
}

/*
 * Inserta en la información relacionada con transiciones “lambda puras” una
 *  nueva entre los estados cuyos nombres se proporcionan como entrada.
 */
AP * APInsertaLTransicion(AP * ap, char * nombre_estado_i, char * nombre_estado_f) {
    int estado_i;
    int estado_f;

    estado_i = APIndiceDeEstado(ap, nombre_estado_i);
    estado_f = APIndiceDeEstado(ap, nombre_estado_f);

    relacionInserta(ap->transiciones_lambda_puras, estado_i, estado_f);
    return ap;
}

/**
 * Inserta en la función de transición guardada en el autómata proporcionado como primer argumento una nueva transición entre los dos estados que se identifican con su nombre (una flecha del diagrama con un símbolo de entrada, un símbolo de pila y una nueva cadena para la cima de la pila representada mediante una palabra )
 * La añade a la transición para el símbolo de la cima de la pila nombre_simbolo_cima_pila, desde el estado de nombre nombre_estado_i al estado nombre_estado_f y con el símbolo de entrada de nombre nombre_simbolo_entrada
 * Respecto al uso de 𝛌 seguiremos el siguiente convenio
 * Cuando quiera utilizarse como argumento (ya sea como símbolo de entrada o como cadena para insertar en la pila) utilizaremos un puntero vacío (NULL) en su lugar.
 */
AP * APInsertaTransicion(AP * ap, char * nombre_simbolo_pila,
        char * nombre_estado_i, char * nombre_estado_f, char* nombre_simbolo_entrada, Palabra * accion) {

    int simbolo_pila;
    int estado_i;
    int estado_f;
    int simbolo_entrada;

    if (nombre_simbolo_entrada != NULL) {
        simbolo_pila = APIndiceDeSimboloPila(ap, nombre_simbolo_pila);
        simbolo_entrada = APIndiceDeSimboloEntrada(ap, nombre_simbolo_entrada);
        estado_i = APIndiceDeEstado(ap, nombre_estado_i);
        estado_f = APIndiceDeEstado(ap, nombre_estado_f);
        transicionAPInsertaAccion(ap->transiciones, simbolo_pila, estado_i, estado_f, simbolo_entrada, accion);
        relacionInserta(ap->transiciones_lambda_puras, estado_i, estado_f);
    } else {
        simbolo_pila = APIndiceDeSimboloPila(ap, nombre_simbolo_pila);
        simbolo_entrada = APIndiceDeSimboloEntrada(ap, ENTRADA_LAMDA);
        estado_i = APIndiceDeEstado(ap, nombre_estado_i);
        estado_f = APIndiceDeEstado(ap, nombre_estado_f);
        transicionAPInsertaAccion(ap->transiciones, simbolo_pila, estado_i, estado_f, simbolo_entrada, accion);
        APInsertaLTransicion(ap, nombre_estado_i, nombre_estado_f);
    }
    APCierraLTransicion(ap);
    return ap;
}

/**
 * 
 * @param pila
 * @param accion
 * @return 
 */
Stack * APInsertaAccionStack(Stack * pila, Palabra * accion) {
    Stack* npila;

    npila = stack_copy(pila);

    /*Sacar primer elemento de pila*/
    void* ele = stack_pop(npila);
    free(ele);
    /*Meter en la pila la palabra en la accion*/
    if (accion != NULL) {
        for (int i = palabraTamano(accion) - 1; i >= 0; i--) {
            stack_push(npila, palabraEnPosicion(accion, i));
        }
    }
    return npila;
}

/*
 palabra = configuracionap
    pilai = pila de 'di'
    estadoi = estado de 'di'
    pos_estadoi = posición en el array del estado en el que está 'di'
    
    desde j=0 hasta j == p_ap->num_estados -1:
        si relacionCierreIJ(p_ap->transiciones_lambda_puras, pos_estadoi, j) == 1:
            estadof = estado destino indicado por 'j'
            aux_di = creo una configuración nueva con estadof, pila, palabra;
            inserto aux_di a la lista conf

    return conf;
 */

ConfiguracionApnd * APInsertaCierreLambda(AP * ap, ConfiguracionApnd * conf, ConfiguracionAp * di) {

    Estado* ei = configuracionApGetEstado(di);
    Palabra* cadena_antes = configuracionApGetPalabra(di);
    Stack* pila = configuracionApGetPila(di);

    char* top_pila = stack_top(pila);
    
    int pos_simbolo_pila = APIndiceDeSimboloPila(ap, top_pila);

    int pos_estadoi = APIndiceDeEstado(ap, estadoNombre(ei));

    int pos_entrada = APIndiceDeSimboloEntrada(ap, ENTRADA_LAMDA);
    /*
        printf("PILA %d ESTADO_I %d  CADENA %d\n", pos_simbolo_pila, pos_estadoi, pos_entrada);
        printf("PILA %s ESTADO_I %s  CADENA %s\n", top_pila, estadoNombre(ei), ENTRADA_LAMDA);
     */
    if (stack_isEmpty(pila) == TRUE) {
        destroy_p_string(top_pila);
        return conf;
    }

    for (int j = 0; j < ap->num_estados; j++) {
        if (relacionCierreIJ(ap->transiciones_lambda_puras, j, pos_estadoi) == 1) {
            List* transicion = list_copy(transicionAPAccionesIJKL(ap->transiciones, pos_simbolo_pila, pos_estadoi, j, pos_entrada));
            while (list_isEmpty(transicion) == FALSE) {
                Palabra* accion = list_extractFirst(transicion);
                Estado* estadof = APEstadoEn(ap, j);
                Stack* pilaf = APInsertaAccionStack(configuracionApGetPila(di), accion);

                ConfiguracionAp* aux_di = configuracionApNueva(estadof, pilaf, cadena_antes);

                //configuracionApImprime(stdout, aux_di);
                configuracionApndInsert(conf, aux_di);
                configuracionApElimina(aux_di);

                stack_destroy(pilaf);
                palabraElimina(accion);
            }
            
            list_destroy(transicion);
        }
    }
    
    destroy_p_string(top_pila);
    return conf;
}

/*
 * Esta función debe realizar sólo un paso de proceso de la cadena actual. Este paso consistirá en lo siguiente
 * Para cada descripción instantánea de la configuración del AP
 * Se aplicarán todas las acciones (transiciones que consuman entrada o modifiquen pila) compatibles con esa descripción instantánea.
 * Para cada una de las descripciones instantáneas conseguidas.
 * Se calculará todas las descripciones instantáneas a las que puede transitar con transiciones no del tipo 𝛌xx.
 * Para cada una de estas descripciones instantáneas debe añadirse también las nuevas descripciones instantáneas obtenidas mediante el siguiente proceso
 * Se mantiene la cadena de entrada de la descripción anterior
 * Se mantiene la pila de la descripción anterior
 * Se añade cada uno de los estados relacionados con el estado de la descripción anterior mediante la relación 𝚲+.
 * El retorno numérico de la función debe entenderse de la siguiente manera
 * Si en esa transición se reconoce la cadena (o por estado final o por vaciado de pila) se devuelve 1
 * En otro caso se devuelve 0.
 * */

int APTransita(AP * ap) {
    int reconocida = 0;
    ConfiguracionApnd* conf_apnd_aux = configuracionApndIni();
    /* CONFIGURACIÓN APND AUXILIAR PARA CADA DESCRIPCIÓN INSTANTANEA DE LA CONFIGURACION ACTUAL 
        mientras la lista de configuraciones actual AP del APND no esté vacía:*/
    while (list_isEmpty(ap->configuracion_actual) == FALSE) {
        int pos_esf = 0;
        ConfiguracionAp* conf = list_extractFirst(ap->configuracion_actual);
        Estado* ei = configuracionApGetEstado(conf);
        Stack* pila = configuracionApGetPila(conf);
        Palabra* cadena_antes = configuracionApGetPalabra(conf);

        if ((stack_isEmpty(pila) == TRUE) && (palabra_is_empty(cadena_antes) == TRUE)) {
            reconocida = 1;
            configuracionApElimina(conf);
            continue;
        }
        if ((estadoTipo(ei) == FINAL) && (palabra_is_empty(cadena_antes) == TRUE)) {
            reconocida = 1;
            configuracionApElimina(conf);
            continue;
        }
        
        if (stack_isEmpty(pila) == TRUE) {
            configuracionApElimina(conf);
            continue;

        }
        if (palabra_is_empty(cadena_antes) == TRUE){
            APInsertaCierreLambda(ap, conf_apnd_aux, conf);
            continue;
        }
        
        int pos_ei = APIndiceDeEstado(ap, estadoNombre(ei));
        char* cima_pila = stack_pop(pila);
        int pos_simbolo_pila = APIndiceDeSimboloPila(ap, cima_pila);
        int pos_entrada = APIndiceDeSimboloEntrada(ap, palabraEnPosicion(cadena_antes, 0));
        Palabra* cadena_despues = palabraCopia_desde_position(cadena_antes, 1);

        for (pos_esf = 0; pos_esf < ap->num_estados; pos_esf++) {

            if (relacionCierreIJ(ap->transiciones_lambda_puras, pos_esf, pos_ei) != 0) {

                //printf("----PILA %d ESTADO_I %d pos_esf %d CADENA %d\n", pos_simbolo_pila, pos_ei, pos_esf, pos_entrada);

                List* transicion;
                transicion = list_copy(transicionAPAccionesIJKL(ap->transiciones,
                        pos_simbolo_pila, pos_ei, pos_esf, pos_entrada));

                // list_print(stdout, transicion);

                while (list_isEmpty(transicion) == FALSE) {

                    Palabra* accion = list_extractFirst(transicion);
                    Estado* estadof = APEstadoEn(ap, pos_esf);
                    Stack* pilaf = APInsertaAccionStack(configuracionApGetPila(conf), accion);
                    ConfiguracionAp* aux_di = configuracionApNueva(estadof, pilaf, cadena_despues);

                    configuracionApndInsert(conf_apnd_aux, aux_di);
                    //configuracionApImprime(stdout, aux_di);
                    //configuracionApndPrint(stdout, conf_apnd_aux);

                    APInsertaCierreLambda(ap, conf_apnd_aux, aux_di);

                    stack_destroy(pilaf);
                    configuracionApElimina(aux_di);
                    palabraElimina(accion);

                }
                list_destroy(transicion);
            }
        }
        palabraElimina(cadena_despues);
        free(cima_pila);
        configuracionApElimina(conf);
    }


    configuracionApndDestroy(ap->configuracion_actual);
    ap->configuracion_actual = conf_apnd_aux;
    return reconocida;
}

/*
 * Invoca a la función “transita” hasta
 * Que no se pueda transitar a ningún sitio más
 * O hasta que la función devuelva 1.
 * En el primer caso se rechaza la cadena y se devuelve un 0 
 * en el segundo se acepta y se devuelve un 1.
Obs.
Las funciones descritas aquí son las obligatorias. Puedes añadir todas aquellas que te faciliten la solución del problema.
Con carácter general, sólo debes asegurarte de cumplir con la interfaz descrita en esta práctica. 
El resto de aspectos de codificación quedan a tu elección.

 */
int APProcesaEntrada(FILE *fd, AP * ap) {
    int iteracion = 0;
    int reconocida = 0;

    printf("SE VA A PROCESAR LA ENTRADA A PARTIR DE ESTA CONFIGURACIÓN INICIAL:");
    configuracionApndPrint(fd, ap->configuracion_actual);

    while (!configuracionApndIsEmpty(ap->configuracion_actual) && (reconocida != 1) && iteracion < 10) {
        if (APTransita(ap) == 1) {
            reconocida = 1;
        }
        printf("ITERACION %d TRAS ITERAR LA CONFIGURACIÓN ES", iteracion);
        configuracionApndPrint(fd, ap->configuracion_actual);
        iteracion++;
    }
    if (reconocida == 1) {
        printf("========>RECONOCIDA\n");
    } else {
        printf("========>RECHAZADA\n");
    }

    return reconocida;
}

