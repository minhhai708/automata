/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include "configuracion_ap.h"

struct _ConfiguracionAp {
    Estado* estado;
    Stack* stack;
    Palabra* palabra;
};

/**
 * @brief Crear una configuracion de AP
 * @param estado
 * @param pila
 * @param cadena
 * @return nueva configuracion de AP
 */
ConfiguracionAp* configuracionApNueva(Estado*estado, Stack*pila, Palabra* cadena) {
    ConfiguracionAp* cap = NULL;
    cap = (ConfiguracionAp*) malloc(sizeof (ConfiguracionAp));
    if (!cap) return NULL;
    cap->estado = estado_copy(estado);
    cap->stack = stack_copy(pila);
    cap->palabra = palabraCopia(cadena);
    return cap;
}

/**
 * @brief Eliminar una configuracion de AP
 * @param p_cap la configuracion
 */
void configuracionApElimina(ConfiguracionAp * p_cap) {
    if (!p_cap) return;
    estadoElimina(p_cap->estado);
    stack_destroy(p_cap->stack);
    palabraElimina(p_cap->palabra);
    free(p_cap);
    return;
}

/**
 * @brief Imprimir la configuracion en la salida indicada
 * @param fd la salida
 * @param p_cap la configuracion
 */
void configuracionApImprime(FILE*fd, ConfiguracionAp* p_cap) {
    if (!p_cap) return;
    printf("(->");
    estadoImprime(fd, p_cap->estado);
    printf("*< ");
    stack_print(fd, p_cap->stack);
    printf(">[");
    palabraImprime(fd, p_cap->palabra);

    printf("])\n");
}

/**
 * @brief Devolver una copia de la configuracion
 * @param p_cap
 * @return la copia
 */

ConfiguracionAp* configuracionApCopia(ConfiguracionAp*p_cap) {
    ConfiguracionAp* newAp = NULL;
    Estado* estado;
    Stack* stack;
    Palabra* palabra;

    estado = estado_copy(p_cap->estado);
    stack = stack_copy(p_cap->stack);
    palabra = palabraCopia(p_cap->palabra);
    newAp = configuracionApNueva(estado, stack, palabra);

    stack_destroy(stack);
    palabraElimina(palabra);
    estadoElimina(estado);

    return newAp;
}

/**
 * @brief Comparar dos configuraciones
 * @param p_cap1
 * @param p_cap2
 * @return 
 */
int configuracionCompara(ConfiguracionAp* p_cap1, ConfiguracionAp*p_cap2) {
    if (!p_cap1 && !p_cap2) return 0;
    if ((p_cap1 != NULL) && (p_cap2 != NULL)) {
        if ((estadoCompara(p_cap1->estado, p_cap2->estado) == 0)&&
            (palabraCompara(p_cap1->palabra, p_cap2->palabra) == 0) && (stack_compare(p_cap1->stack, p_cap2->stack)== 0) )
            return 0;
    }
    return 1;
}

/**
 * @brief Devolver la palabra de la configuracion
 * @param ap
 * @return 
 */
Palabra* configuracionApGetPalabra(ConfiguracionAp* ap) {
    return ap->palabra;
}

/**
 * @brief Devolver la pila de la configuracion
 * @param ap
 * @return 
 */
Stack* configuracionApGetPila(ConfiguracionAp* ap) {
    return ap->stack;
}

/**
 * @brief Devolver el estado de la configuracion
 * @param ap
 * @return 
 */
Estado* configuracionApGetEstado(ConfiguracionAp* ap){
    return ap->estado;
}