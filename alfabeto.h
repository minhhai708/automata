#ifndef _ALFABETO_H
#define _ALFABETO_H

#include <stdio.h>

typedef struct _Alfabeto Alfabeto;

Alfabeto * alfabetoNuevo(char * nombre, int tamano);
void alfabetoElimina(Alfabeto * p_alfabeto);
Alfabeto * alfabetoInsertaSimbolo(Alfabeto * p_alfabeto, char * simbolo);
void alfabetoImprime(FILE * fd, Alfabeto * p_alf);
char* alfabetoSimboloEn(Alfabeto * p_alf, int i);
int alfabetoIndiceDeSimbolo(Alfabeto * p_alf, char * simbolo);
int alfabetoTamano(Alfabeto * p_alf);
/* PARA LA PRÁCTICA 3 */
Alfabeto * alfabetoUne(Alfabeto * p_a1, Alfabeto * p_a2);

#endif
