/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   configuracion_apnd.h
 * Author: temporal
 *
 * Created on 19 de octubre de 2017, 16:51
 */

#ifndef CONFIGURACION_APND_H
#define CONFIGURACION_APND_H

#ifdef __cplusplus
extern "C" {
#endif
    
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "estado.h"
#include "palabra.h"
#include "stack.h"
#include "configuracion_ap.h"
#include "list.h"
#include "generic_collections_types.h"
#include "basic_types.h"
#include "generic_string.h"
    
typedef List ConfiguracionApnd;

/**
 * @brief Inicializa una configuración no determinista
 * @param destroy
 * @param copy
 * @param print
 * @param compare
 * @return 
 */
ConfiguracionApnd * configuracionApndIni();
/**
 * @brief Inserta una configuración determinista en una no determinista. 
 * Se realiza una copia en memoria propia de la colección para el nuevo elemento y se asegura que no haya duplicados
 * @param capnd
 * @param p_cap
 * @return 
 */
ConfiguracionApnd* configuracionApndInsert(ConfiguracionApnd* capnd, const ConfiguracionAp * p_cap);
/**
 * @brief  Se obtiene una configuración determinista de la no determinista, que desaparece de la colección.
 * No se especifica el orden en el que se extrae
 * @param capnd
 * @return 
 */
ConfiguracionAp * configuracionApndExtract(ConfiguracionApnd * capnd);
/**
 * @brief  Se devuelve 1 si está vacía y 0 en caso contrario
 * @param capnd
 * @return 
 */
int configuracionApndIsEmpty(const ConfiguracionApnd* capnd);
/**
 * @brief  Se devuelve el número de configuraciones deterministas que hay dentro de la no determinista

 * @param capnd
 * @return 
 */
int configuracionApndSize(const ConfiguracionApnd * capnd);
/**
 * @brief  Se imprime todas las configuraciones deterministas. No se especifica en qué orden
 * @param fd
 * @param capnd
 * @return 
 */
int configuracionApndPrint(FILE *fd, const ConfiguracionApnd* capnd);
/**
 * @brief Se libera toda la memoria asociada con la configuracion no determinista 
 * @param capnd
 */
void configuracionApndDestroy( ConfiguracionApnd* capnd);

#ifdef __cplusplus
}
#endif

#endif /* CONFIGURACION_APND_H */

