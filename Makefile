
CC = gcc -ansi -pedantic -lm
CFLAGS = -Wall -std=c99
EXE = p1
TYPES = generic_collections_types.h basic_types.h 
OBJECTS = main.o configuracion_apnd.o configuracion_ap.o stack.o estado.o palabra.o generic_string.o list.o dynamic_node.o matrix.o relacion.o alfabeto.o ap.o transicion_ap.o
all: $(EXE)
	./p1
.PHONY : clean

dynamic_node.o: dynamic_node.c dynamic_node.h $(TYPES)
	@echo "#---------------------------"
	@echo "# Generando $@ "
	$(CC) $(CFLAGS) -c $< -o $@
	
list.o: list.c list.h dynamic_node.h $(TYPES)
	@echo "#---------------------------"
	@echo "# Generando $@ "
	$(CC) $(CFLAGS) -c $< -o $@
		
matrix.o: matrix.c matrix.h matrix.h $(TYPES)
	@echo "#---------------------------"
	@echo "# Generando $@ "
	$(CC) $(CFLAGS) -c $< -o $@
	
generic_string.o: generic_string.c generic_string.h $(TYPES)
	@echo "#---------------------------"
	@echo "# Generando $@ "
	$(CC) $(CFLAGS) -c $< -o $@

palabra.o: palabra.c palabra.h $(TYPES)
	@echo "#---------------------------"
	@echo "# Generando $@ "
	$(CC) $(CFLAGS) -c $< -o $@
	
estado.o: estado.c estado.h $(TYPES)
	@echo "#---------------------------"
	@echo "# Generando $@ "
	$(CC) $(CFLAGS) -c $< -o $@
	
stack.o: stack.c stack.h $(TYPES)
	@echo "#---------------------------"
	@echo "# Generando $@ "
	$(CC) $(CFLAGS) -c $< -o $@
	
configuracion_ap.o: configuracion_ap.c configuracion_ap.h stack.h palabra.h estado.h $(TYPES)
	@echo "#---------------------------"
	@echo "# Generando $@ "
	$(CC) $(CFLAGS) -c $< -o $@
	
configuracion_apnd.o: configuracion_apnd.c configuracion_apnd.h configuracion_ap.h stack.h palabra.h estado.h $(TYPES)
	@echo "#---------------------------"
	@echo "# Generando $@ "
	$(CC) $(CFLAGS) -c $< -o $@

relacion.o: relacion.c relacion.h matrix.h $(TYPES)
	@echo "#---------------------------"
	@echo "# Generando $@ "
	$(CC) $(CFLAGS) -c $< -o $@
	
alfabeto.o: alfabeto.c alfabeto.h $(TYPES)
	@echo "#---------------------------"
	@echo "# Generando $@ "
	$(CC) $(CFLAGS) -c $< -o $@
	
transicion_ap.o: transicion_ap.c transicion_ap.h list.h $(TYPES)
	@echo "#---------------------------"
	@echo "# Generando $@ "
	$(CC) $(CFLAGS) -c $< -o $@
	
ap.o: ap.c ap.h alfabeto.h estado.h relacion.h configuracion_apnd.h transicion_ap.h list.h generic_string.h $(TYPES)
	@echo "#---------------------------"
	@echo "# Generando $@ "
	$(CC) $(CFLAGS) -c $< -o $@

main.o: main.c generic_string.h stack.h palabra.h estado.h ap.h relacion.h $(TYPES)
	@echo "#---------------------------"
	@echo "# Generando $@ "
	$(CC) $(CFLAGS) -c $< -o $@
	
p1: $(OBJECTS)
	@echo "#---------------------------"
	@echo "# Generando $@ "
	$(CC) $^ -Wall $(LIBS) -o $@

########################################################################
# Using utilities
########################################################################
# $ make astyle-c
astyle-c:
	@echo "Applying Artistic Style to C programming files."
	@astyle *.h *.c
	
doxygen:
	@rm -fr Doxyfile latex html
	@doxygen -g
	@doxygen Doxyfile
	
upload: 	
	@rm -f *~ *.o
	git add .
	git commit -m "upload"
	git push
valgrind:
	valgrind --leak-check=yes ./p1
########################################################################
# Information and Clean
########################################################################
info:
	@echo
	@echo "<AUTOMATA Y LENGUAJE>, <GRADO>"
	@echo "<Codigo de la entrega>"
	@echo
	@echo "Autores:"
	@echo "	<MINH HAI LE, MINH.LE@ESTUDIANTE.UAM.ES>"
	@echo "	<GONZALO, Correo electronico>"
	
clean:
	@echo "Borrando objetos, ejecutables, etc."
	@rm -fr Doxyfile latex html
	@rm -f *~ *.o core $(EXE)
