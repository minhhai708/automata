/**
 * 
 */
#include "stack.h"

struct _Stack {
    int top;
    void** item;
    destroy_element_function_type destroy_element_function;
    copy_element_function_type copy_element_function;
    print_element_function_type print_element_function;
    cmp_element_function_type cmp_element_function;
};

/**
 * Inicializa​la pila reservando​memoria y almacenando lostres​ ​ punteros​ ​ afunción pasados como​
 * parámetro (el primero para destruir​elementos, el segundo para copiar elementos​y el tercero​para
 * imprimir​elementos).
 * @param 
 */
Stack * stack_ini(destroy_element_function_type fdestroy, copy_element_function_type fcopy,
        print_element_function_type fprint, cmp_element_function_type fcmp) {
    Stack* pila = NULL;
    pila = malloc(sizeof (Stack));
    if (!pila) return NULL;
    pila->item = (void **) malloc(sizeof (void*));
    pila->top = 0;
    pila->destroy_element_function = fdestroy;
    pila->copy_element_function = fcopy;
    pila->print_element_function = fprint;
    pila->cmp_element_function = fcmp;
    return pila;
}

/**
 * Eliminar la pila
 * @param la pila para eliminar
 */
void stack_destroy(Stack *stack) {
    int i;
    if (!stack) return;
    for (i = 0; i < stack->top; i++) {
        stack->destroy_element_function(stack->item[i]);

    }
    free(stack->item);
    free(stack);
    return;
}

/**
 * insertar un elemento en la pila
 * @param stack la pila para eliminar
 * @param item el item que va a meter en la pila
 * @return devolver stack despues de push
 */

Stack* stack_push(Stack* stack, const void* item) {
    void** newItem;
    if (!stack || !item) return NULL;

    newItem = (void **) realloc(stack->item, (stack->top + 1) * sizeof (void*));
    if (newItem) {
        void* newEle;
        stack->item = newItem;
        newEle = stack->copy_element_function(item);
        if (newEle) {
            stack->item[stack->top] = newEle;
            stack->top = stack->top + 1;
            /*printf("PUSH %s", item);*/
        }
    }
    return stack;
}

/**
 * Extraer un elemento en la pila
 * @param stack la pila
 * @return 
 */


void* stack_pop(Stack* stack) {
    int top = 0;
    void* itemExtraido = NULL;
    if (!stack) return NULL;
    if (stack_isEmpty(stack) == TRUE) {
        return NULL;
    }
    top = stack->top;
    itemExtraido = stack->copy_element_function(stack->item[top - 1]);
    stack->destroy_element_function(stack->item[top - 1]);
    stack->item = realloc(stack->item, (top - 1) * sizeof (void*));
    stack->top--;
    return itemExtraido;
}


/**
 * Copiar un elemento sin modificar la pila
 * @param stack la pila
 * @return NULL si no logra, el elemento si lo logra
 */
void* stack_top(const Stack* stack) {
    int top = 0;
    void *itemTop = NULL;

    if (!stack) return NULL;
    if (stack_isEmpty(stack) == TRUE) return NULL;
    top = stack->top;
    itemTop = stack->copy_element_function(stack->item[top - 1]);
    return itemTop;
}

/**
 * 
 * @param stack
 * @return 
 */
Bool stack_isEmpty(const Stack*stack) {
    if (!stack) return TRUE;
    if (stack->top == 0) return TRUE;
    else return FALSE;
}

/**
 * 
 * @param file
 * @param stack
 * @return 
 */
int stack_print(FILE* file, const Stack* stack) {
    int i;
    int numChar = 0;

    if (!file || !stack) return -1;

    if (stack_isEmpty(stack) == TRUE) return 0;

    for (i = 0; i < stack->top; i++) {
        int aux;
        aux = stack->print_element_function(file, stack->item[i]);
        numChar = numChar + aux;
    }

    return numChar;
}

/**
 * Tamaño de la pila
 * @param stack la pila
 * @return numero de elemento en la pila ( 0 si esta vacio, -1 si hay algun error
 */
int stack_size(const Stack* stack) {
    if (!stack) return ERROR;
    else return stack->top;
}

/**
 * 
 * @param pila
 * @return 
 */
Stack* stack_copy(Stack* pila) {
    Stack * p_copy = NULL;
    int i;
    if (!pila) return NULL;

    p_copy = stack_ini((destroy_element_function_type) pila->destroy_element_function,
            (copy_element_function_type) pila->copy_element_function,
            (print_element_function_type) pila->print_element_function,
            (cmp_element_function_type) pila->cmp_element_function);

    for (i = 0; i < pila->top; i++) {
        stack_push(p_copy, pila->item[i]);
    }
    return p_copy;
}

int stack_compare(Stack* s1, Stack *s2) {
    int i;
    if (!s1 && !s2) return 0;
    if (s1->top != s2->top) return 1;
    for (i = 0; i < s1->top; i++) {
        if (s1->cmp_element_function(s2->item[i], s1->item[i]) != 0)
            return 1;
    }
    return 0;
}