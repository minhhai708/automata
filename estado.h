/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   estado.h
 * Author: MINH HAI LE
 *
 * Created on 19 de octubre de 2017, 16:51
 */

#ifndef ESTADO_H
#define ESTADO_H

#ifdef __cplusplus
extern "C" {
#endif
#define INICIAL 0
#define FINAL 1
#define INICIAL_Y_FINAL 2
#define NORMAL 3

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "generic_collections_types.h"
#include "basic_types.h"
    typedef struct _Estado Estado;
    /**
     * @brief Crear un estado nuevo
     * @param nombre
     * @param tipo
     * @return 
     */
    Estado* estadoNuevo(const char* nombre, int tipo);
    /**
     * @brief Eliminar un estado y liberar su memoria
     * @param p_s Estado
     */
    void estadoElimina(Estado * p_s);
    /**
     * @brief Imprimir el estado 
     * @param fd
     * @param p_s
     */
    void estadoImprime(FILE* fd, Estado* p_s);
    /**
     * @brief Función de comparación de estados por nombre, devuelve 1 si el estado tiene el nombre proporcionado
     * @param p_s el estado
     * @param nombre el nombre
     * @return 
     */

    int estadoEs(Estado * p_s, char* nombre);
    /**
     * @brief Dado un estado y devolver su nombre
     * @param p_s el estado
     * @return nombre del estado
     */
    char * estadoNombre(Estado* p_s);

    /**
     * @brief Dado un estado y devolver su tipo
     * @param p_s
     * @return el tipo del estado
     */
    int estadoTipo(Estado * p_s);

    /**
     * @brief Se usa para comparar estados por su nombre
     * @param p_s1
     * @param p_s2
     * @return el strcmp de sus nombres
     */
    int estadoCompara(Estado * p_s1, Estado * p_s2);

    /**
     * @brief Se crea una copia en memoria nueva del estado proporcionado como argumento
     * @param p_s
     * @return la copia del estado
     */
    Estado * estado_copy(Estado * p_s);

#ifdef __cplusplus
}
#endif

#endif /* ESTADO_H */

